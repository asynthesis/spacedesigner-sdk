## Modules

#### Structure
```
YourModule (PascalCase name)
  |- config
  |    |- EditorConfig.tsx
  |    |    (handles configuration and registration of the entity in EditorMain)
  |    |    (also registers property panels and custom tabs if needed)
  |    |- ViewerConfig.tsx
  |    |    (registers the entity in sd viewer)
  |    |- IYourEntityConfigData.ts
  |    |    (use this if you need to extend IConfigData with custom default props)
  |    |- ...
  |         (other configuration files, interface declarations, etc if needed)
  |- sdeditor
  |    |- YourEntityPanel.tsx
  |    |    (property panel for your entity if needed)
  |    |- YourEntityTab.tsx
  |    |    (custom tab for your entity if needed)
  |    |- ...
  |         (other files can go here, but they should not be accessible from sdviewer)
  |- sdlib
  |    |- YourEntity-config.ts
  |    |    (holds every configuration needed for a given entity, very important)
  |    |- YourEntity.ts
  |    |- YourEntityShape2D.ts
  |    |- YourEntityShape3D.ts
  |    |- YourEntityHandle2D.ts
  |    |- YourEntityHandle3D.ts
  |    |- ...
  |         (nothing in this folder should depend on sdeditor or sdviewer!)
  |- sdviewer
       (viewer-specific code if needed, should not depend on sdeditor)

```


## Usage
1. Clone this repository
2. Run `npm install`
3. Create a self-signed certificate in the root directory, e.g. with `openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem`
4. Build the environment and the template module with `npm run build`
5. Start your local web server with `npm run start`
6. Go to `https://localhost:8080` in your browser, the environment should be ready

The environment uses two npm packages from Asynth: Space Designer Library (`sdlib`) is the core of the application, which handles the object creation and scene rendering; Space Designer Editor (`spacedesigner`) is a React-based application which is responsible for the UI components. Not every version of `sdlib` is compatible with the latest `spacedesigner` package, so `sdlib` is loaded as a dependency of the main module to ensure compatible versions. During development, some classes need to be imported from the `sdlib` package, so in order for auto-imports to work properly in IDEs, please make sure the package is marked as `not excluded`.


## Deployment
After building, the module-specific output bundle(s) will be ready for deployment under `spacedesigner-sdk/dist`. Modules need to have a bundle for sdeditor (the main Space Designer application), and another bundle will be created if the module also needs to be available in sdviewer.
The generated bundle files need to end up under the following path on the live server:

`/htdocs/releases/{BRANCH_NAME_OR_COMMIT_SLUG}/lib/spacedesigner/modules/`

Currently, there is no automated way to deploy the SDK modules. To deploy, upload the module bundle file(s) to the specific folder using FTP.

This is not enough though, as he next deployment from CI/CD will overwrite the release folder and will delete the modules. To solve this, upload the module to the following path too:

`/htdocs/data/sites/spacedesigner/sdk-modules`

The deployment script will search this folder and copy over the module bundles every time there is a new master deploy.

**Note: this only works on Space Designer main and dev server!**


## Potential problems

There is no way to select specific versions of the app per-whitelabel. If modules are developed in the SDK, they might break when a new app version is released.

E.g. if the Space Designer version in the SDK is 4.169.3, and some modules are developed using that, there is no way to know if those modules are compatible with Space Designer 4.170.0 without rebuilding and manually testing them. Especially since the new version might just be released silently on the server.

Before SDK modules can actually be used in production, it would be nice to find a proper solution for this.