const WebpackConfig = require("webpack-config").Config;
const WebpackBuildNotifierPlugin = require('webpack-build-notifier');

const watchConfig = {
	watch: true,
	plugins: [new WebpackBuildNotifierPlugin({
		title: "Space Designer SDK",
		sound: false,
		suppressSuccess: true // only show success notification for initial successful compilation and after failed compilations.
	})]
};
module.exports = new WebpackConfig().extend("webpack.config.js").merge(watchConfig);
