const WebpackConfig = require("webpack-config").Config;

const config = {
	mode: "development",
	entry: {
		index: "./src/index.tsx",

		// register example modules to compile, load and test
		sdeditor_Template: "./src/Template/config/EditorConfig.tsx",
		sdviewer_Template: "./src/Template/config/ViewerConfig.tsx",
		sdeditor_SimpleNote: "./src/SimpleNote/config/EditorConfig.tsx",
		sdviewer_SimpleNote: "./src/SimpleNote/config/ViewerConfig.tsx",
		sdeditor_SimplePolygon: "./src/SimplePolygon/config/EditorConfig.tsx",
		sdviewer_SimplePolygon: "./src/SimpleNote/config/ViewerConfig.tsx",
		sdeditor_SimplePrimitive: "./src/SimplePrimitive/config/EditorConfig.tsx",
		sdviewer_SimplePrimitive: "./src/SimplePrimitive/config/ViewerConfig.tsx"
	},
	output: {
		path: __dirname + "/dist",
		filename: "[name].js"
	},
	devtool: 'source-map',
	resolve: {
		extensions: [".ts", ".tsx", ".js", ".json"]
	},
	module: {
		rules: [
			{
				// all files with a '.ts' or '.tsx' extension will be handled by ts-loader
				test: /\.tsx?$/,
				use: [{loader: 'ts-loader', options: {onlyCompileBundledFiles: true}}]
			},
			{
				// js files will have sourcemaps re-processed by source-map-loader
				enforce: "pre",
				test: /\.js$/,
				loader: "source-map-loader"
			},
			{
				// image assets will be inlined in the bundle
				test: /\.(png|jpg|jpeg|svg)$/i,
				type: "asset/inline",
			},
			{
				test: /\.styl$/i,
				use: [
					"style-loader",
					{loader: "css-loader", options: {sourceMap: true}},
					{loader: "stylus-loader", options: {sourceMap: true}}
				],
			}
		]
	},
	externals: {
		'react': 'React',
		'react-dom': 'ReactDOM',
		'sdlib': 'SDLib',
		'spacedesigner': 'SDEditor'
	}
};

module.exports = new WebpackConfig().merge(config);
