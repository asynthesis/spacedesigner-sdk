import * as React         from "react";
import * as ReactDOM      from "react-dom";
import {EditorMainLoader} from "spacedesigner";

// parameters for SpaceDesigner3D application run and testing
const loaderParams = {
	// url to save/load project from, login functionality
	"apiRoot": "https://www.spacedesigner3d.com/",

	// project key to load
	"projectKey": "7f0d3f201657a4ae120f2c3a6d25f6915ca615bb",
	
	// url to access cdn cached asset s
	"cdn": "https://www.spacedesigner3d.com/",

	// default data and resources folder routes
	"commonDataFolder": "/data/main/",
	"customDataFolder": "/data/sites/spacedesigner/",
	"commonResourceFolder": "/resources/",
	"customResourceFolder": "/sites/spacedesigner/resources/",

	// url to libary and translation files to load
	"url_content": "https://www.spacedesigner3d.com/data/sites/spacedesigner/caches/content_en.json",
	"url_library": "https://www.spacedesigner3d.com/data/sites/spacedesigner/caches/library_en.json",

	// css files to load
	"url_editor_css": "./node_modules/spacedesigner/css/editor.css",
	"url_mobile_css": "./node_modules/spacedesigner/css/viewer.css",
	"url_ui_assets": "./node_modules/spacedesigner/assets",
	"url_worker_js": "./node_modules/sdlib/sdworker.js",
	"url_modules": "./dist",

	// turn off connection checker for testing purpose
	"connectionChecker": {"url": null},

	// application language to load
	"language": "en",

	// custom wall type definitions, if needed
	"wallTypes": [],

	// left panels to load and show in order
	"panel_design": 0,
	"panel_catalog": 1,
	"panel_background": 2,
	"panel_print": 3,
	"panel_config": 4,
	"panel_sun": 5,
	"panel_about": 6,
	"panel_template": 7,
	"panel_simplenote": 8,
	"panel_simpleprimitive": 9,

	// modules to load and work with
	"modules": ["Template", "SimpleNote", "SimplePolygon", "SimplePrimitive"]
};

// create editor app and load project
const loader = <EditorMainLoader data={loaderParams}/>;
const target = document.getElementById("sd-editor-loader");

// render app
ReactDOM.render(loader, target);
