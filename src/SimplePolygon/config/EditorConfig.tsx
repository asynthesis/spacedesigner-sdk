import * as React                                       from "react";
import {EntityPanels, ModuleTools}                      from "spacedesigner";
import {BrowserUtils, EntityConfiguration}              from "sdlib";
import {SimplePolygonPanel}                             from "../sdeditor/SimplePolygonPanel";
import {SimplePolygon}                                  from "../sdlib/SimplePolygon";
import {SimplePolygonConfig}                            from "../sdlib/SimplePolygon-config";
import {SimplePolygonToolMouse, SimplePolygonToolTouch} from "../sdlib/SimplePolygonTool";

EntityConfiguration.addEntityConfiguration(SimplePolygonConfig);

EntityPanels.setPanelForEntity(SimplePolygon.TYPE, (entity: SimplePolygon) => <SimplePolygonPanel entity={entity} key={entity.uid}/>);

ModuleTools.addModuleTool({
	id: "SimplePolygon",
	toolClass: BrowserUtils.isMobile() ? SimplePolygonToolTouch : SimplePolygonToolMouse,
	label: "SimplePolygon",
	toolTipLabel: "SimplePolygon",
	iconUrl: "/img/editor/tabs/wallCurved.svg"
});
