import * as React          from "react";
import {
	C,
	Material
}                          from "sdlib";
import {
	EditorMain,
	Label,
	MaterialRenderer,
	MaterialDialog,
	PropanelContainer
}                          from "spacedesigner";
import {SimplePolygon}     from "../sdlib/SimplePolygon";

interface ISimplePolygonPanelProps
{
	entity: SimplePolygon;
}

interface ISimplePolygonPanelState
{
	material: Material;
}

export class SimplePolygonPanel extends React.PureComponent<ISimplePolygonPanelProps, ISimplePolygonPanelState>
{
	public constructor(props: ISimplePolygonPanelProps)
	{
		super(props);
		this.state = {
			material: this.props.entity.material,
		};
	}

	public componentDidUpdate(): void
	{
		// update the entity material
		this.props.entity.material = this.state.material;
	}

	private readonly onMaterialClick = (): void =>
	{
		const {entity} = this.props;

		// show material dialog
		EditorMain.instance.showDialogBox(<MaterialDialog
			material={entity.material}
			applyCallback={this.onMaterialChange}
		/>);
	};

	private readonly onMaterialChange = (material: Material): void =>
	{
		// update the selected material from material dialog
		this.setState({material: material});
	};

	public render(): JSX.Element
	{
		const { entity } = this.props;
		return (
			<PropanelContainer
				className="sd-surfacepanel"
				entity={entity}
				enableDuplicate={true}
				enableDelete={true}
			>
				<div className="title">
					{C('simple polygon')}
				</div>

				<div className="hline"/>

				<div className="hbox">
					<Label text={C('top material')}/>
					<MaterialRenderer
						material={this.state.material}
						onClick={this.onMaterialClick}
						onSampleClick={this.onMaterialChange}

					/>
				</div>
			</PropanelContainer>
		);
	}
}
