import {
	GeomUtils,
	Point2D,
	Property,
	IEntityData
} from                    "sdlib";
import {SimplePolygon}     from "./SimplePolygon";

export interface ISimplePolygonCornerData extends IEntityData
{
	simplePolygon?: SimplePolygon;
	x: number;
	y: number;
}

export class SimplePolygonCorner
{
	public static SNAP_TOLERANCE: number = 15;

	private readonly _x = new Property<number>(0);
	private readonly _y = new Property<number>(0);

	private readonly _simplePolygon: SimplePolygon;

	public constructor(data: ISimplePolygonCornerData)
	{
		if (!data)
		{
			return;
		}

		this._x.apply(data.x);
		this._y.apply(data.y);
		this._simplePolygon = data.simplePolygon;
	}

	// ITransformable -----------------------------------------------------

	public moveTo(coord: Point2D): void
	{
		this._x.apply(coord.x);
		this._y.apply(coord.y);

		this._simplePolygon.invalidate();
	}

	public moveDiff(delta: Point2D): void
	{
		this._x.value += delta.x;
		this._y.value += delta.y;

		this._simplePolygon.invalidate();
	}

	public rotate(angle: number, reference: Point2D = null): void
	{
		if (reference === null)
		{
			return;
		}

		const p: Point2D = GeomUtils.rotate(this.p, angle, reference);

		this._x.apply(p.x);
		this._y.apply(p.y);

		this._simplePolygon.invalidate();
	}

	public mirror(p0: Point2D, p1: Point2D): void
	{
		const p = GeomUtils.mirror(this.p, p0, p1);
		this._x.apply(p.x);
		this._y.apply(p.y);
		this._simplePolygon.invalidate();
	}

	// --------------------------------------------------------------------------------------------------
	// Getters, setters

	public get prev(): SimplePolygonCorner
	{
		return (this._simplePolygon) ? this._simplePolygon.getPrevCorner(this) : null;
	}

	public get next(): SimplePolygonCorner
	{
		return (this._simplePolygon) ? this._simplePolygon.getNextCorner(this) : null;
	}

	public get x(): number
	{
		return this._x.value;
	}

	public set x(value: number)
	{
		this._x.apply(value);

		this._simplePolygon.invalidate();
	}

	public get y(): number
	{
		return this._y.value;
	}

	public set y(value: number)
	{
		this._y.apply(value);

		this._simplePolygon.invalidate();
	}

	public get p(): Point2D
	{
		return new Point2D(this._x.value, this._y.value);
	}

	public set p(value: Point2D)
	{
		this._x.apply(value.x);
		this._y.apply(value.y);
		this._simplePolygon.invalidate();
	}

	public get simplePolygon(): SimplePolygon
	{
		return this._simplePolygon;
	}
}
