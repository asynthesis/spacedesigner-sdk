import {
	BoolProperty,
	BoxEntity,
	GeomUtils,
	IEntityData,
	IInventorable,
	inventorable,
	InventoryVO,
	IPoint2D,
	IMaterialData,
	Material,
	MaterialVO,
	Point2D,
	Rectangle2D,
	Scene
}                            from "sdlib";
import {SimplePolygonCorner} from "./SimplePolygonCorner";

export interface ISimplePolygonData extends IEntityData
{
	points       : IPoint2D[];
	material?    : IMaterialData;
}

@inventorable
export class SimplePolygon extends BoxEntity implements IInventorable
{
	public static TYPE = "SimplePolygon";

	public static DEFAULT_MATERIAL_ID = 594;

	private readonly _corners: SimplePolygonCorner[] = [];
	private readonly _includeInInventory = new BoolProperty(true);
	private _material  : Material;

	public static Init(scene: Scene, data?: ISimplePolygonData): SimplePolygon
	{
		return <SimplePolygon> super.Init(scene, data);
	}

	// ITransformable -----------------------------------------------------

	public clone(offset?: IPoint2D, levelId?: string, versionId?: string): SimplePolygon
	{
		const data = this.serialize();
		if (offset)
		{
			for (const point of data.points)
			{
				point.x += offset.x;
				point.y += offset.y;
			}
		}
		if (levelId)
		{
			data.level = levelId;
		}
		if (versionId)
		{
			data.version = versionId;
		}

		this.scene.ids.transformUids(data);

		return <SimplePolygon> SimplePolygon.Init(this.scene, data);
	}

	/**
	 * Moves the corners that the center of the simple polygon will be at position 'coord'
	 * @param coord
	 */
	public moveTo(coord: Point2D): void
	{
		const delta: Point2D = coord.subtract(this.center);
		this.moveDiff(delta);
	}

	/**
	 * Moves every corner with 'delta'
	 * @param delta
	 */
	public moveDiff(delta: Point2D): void
	{
		for (const corner of this._corners)
		{
			corner.moveDiff(delta);
		}
	}

	/**
	 * Rotates the corners of the line with 'angle' degree around 'reference'
	 * @param angle
	 * @param reference
	 */
	public rotate(angle: number, reference: Point2D = null): void
	{
		for (const corner of this._corners)
		{
			corner.rotate(angle, reference);
		}
	}

	// We need to implement rotation from ITransformable
	public get rotation(): number
	{
		return 0;
	}

	public get z(): number
	{
		return 0;
	}

	public set z(value: number)
	{
		return;
	}

	public mirror(p0: Point2D, p1: Point2D): void
	{
		for (const corner of this._corners)
		{
			corner.mirror(p0, p1);
		}
	}

	public stretch(delta: Point2D): void
	{
		this.moveDiff(delta);
	}

	public groupMirror(p0: Point2D, p1: Point2D): void
	{
		this.mirror(p0, p1);
	}

	public groupMove(delta: Point2D): void
	{
		this.moveDiff(delta);
	}

	public groupRotate(angle: number, reference: Point2D = null): void
	{
		this.rotate(angle, reference);
	}

	public serializeLibrary(): { materials: MaterialVO[] }
	{
		// return the material ids used by this entity, needed for viewer
		return {
			materials: [this._material.materialVO]
		};
	}

	public serialize(): ISimplePolygonData
	{
		const data = <ISimplePolygonData> super.serialize();
		data.points = [];
		data.material = this._material.serialize();

		for (const corner of this._corners) data.points.push(new Point2D(corner.x, corner.y));

		return data;
	}

	public deserialize(data: ISimplePolygonData): void
	{
		super.deserialize(data);

		this._material = (data.material) ? new Material(data.material) : new Material({id: SimplePolygon.DEFAULT_MATERIAL_ID});

		if (data.points)
		{
			for (const p of data.points)
			{
				this._corners.push(new SimplePolygonCorner({ x: p.x, y: p.y, simplePolygon: this }));
			}
		}

		this.invalidate();
	}

	/**
	* Returns the previous corner
	*/
	public getPrevCorner(corner: SimplePolygonCorner): SimplePolygonCorner
	{
		let i = this._corners.indexOf(corner) - 1;
		i = (this._corners.length + i) % this._corners.length;
		return this._corners[i];
	}

	/**
	* Returns the next corner
	*/
	public getNextCorner(corner: SimplePolygonCorner): SimplePolygonCorner
	{
		let i = this._corners.indexOf(corner) + 1;
		i %= this._corners.length;
		return this._corners[i];
	}

	/**
 	* Returns the smallest rectangle encompassing the surface
	* @returns {Rectangle2D}
	*/
	public getBoundingBox(): Rectangle2D
	{
		// searching for smallest and biggest coordinates to create a bounding box
		const corners = this.corners;
		let min: Point2D;
		let max: Point2D;
	
		if (corners.length > 0)
		{
			min = new Point2D(corners[0].x, corners[0].y);
			max = new Point2D(corners[0].x, corners[0].y);
		}
		for (let i = 1; i < corners.length; i++)
		{
			if (corners[i].y < min.y)
			{
				min.y = corners[i].y;
			}
			if (corners[i].y > max.y)
			{
				max.y = corners[i].y;
			}
		
			if (corners[i].x < min.x)
			{
				min.x = corners[i].x;
			}
			if (corners[i].x > max.x)
			{
				max.x = corners[i].x;
			}
		}
	
		return new Rectangle2D(min.x, min.y, Math.abs(max.x - min.x), Math.abs(max.y - min.y));
	}

	/**
	* Returns the area of the surface
	*/
	public get area(): number
	{
		return GeomUtils.PolygonArea(this.points);
	}
 
	/**
	 * Returns the centroid of the surface
	*/
	public get center(): Point2D
	{
		return GeomUtils.Centroid(this.points);
	}

	/**
	* Extracts the points from the corners of the surface
	* @returns {Array}
	*/
	public get points(): Point2D[]
	{
		return this._corners.map(corner => new Point2D(corner.x, corner.y));
	}

	public get p(): Point2D
	{
		const rect = this.getBoundingBox();
		return new Point2D(
			rect.x + rect.width / 2,
			rect.y + rect.height / 2
		);
	}

	public set p(value: Point2D)
	{
		const rect = this.getBoundingBox();
		const middle = new Point2D(
			rect.x + rect.width / 2,
			rect.y + rect.height / 2
		);
		const diff = new Point2D(value.x - middle.x, value.y - middle.y);
		this.corners.forEach(corner => corner.moveDiff(diff));
		this.invalidate();
	}

	public get x(): number
	{
		const rect = this.getBoundingBox();
		return rect.x + rect.width / 2;
	}

	public set x(value: number)
	{
		const rect = this.getBoundingBox();
		const middle = rect.x + rect.width / 2;
		const diff = value - middle;
		this.corners.forEach(corner => corner.x += diff);
		this.invalidate();
	}

	public get y(): number
	{
		const rect = this.getBoundingBox();
		return rect.y + rect.height / 2;
	}

	public set y(value: number)
	{
		const rect = this.getBoundingBox();
		const middle = rect.y + rect.height / 2;
		const diff = value - middle;
		this.corners.forEach(corner => corner.y += diff);
		this.invalidate();
	}

	public get height(): number
	{
		return this._height.value;
	}

	public set height(value: number)
	{
		this._height.apply(value);
		this.invalidate();
	}

	public getInventoryInfo(): InventoryVO[]
	{
		const inventory: InventoryVO[] = [];
		return inventory;
	}

	public get includeInInventory(): boolean
	{
		return this._includeInInventory.value;
	}

	public set includeInInventory(value: boolean)
	{
		this._includeInInventory.apply(value);
	}

	public get corners(): SimplePolygonCorner[]
	{
		return this._corners;
	}

	public get material(): Material
	{
		return this._material;
	}

	public set material(value: Material)
	{
		this._material = value;
		this.invalidate();
	}
}
