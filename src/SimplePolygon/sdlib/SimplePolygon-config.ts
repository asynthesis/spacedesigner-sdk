import {IEntityConfiguration}         from "sdlib";
import {SimplePolygon}                from "./SimplePolygon";
import {SimplePolygonHandle2D}        from "./SimplePolygonHandle2D";
import {SimplePolygonShape2D}         from "./SimplePolygonShape2D";
import {SimplePolygonShape3D}         from "./SimplePolygonShape3D";
import "../assets/css/main.styl";

export const SimplePolygonConfig: IEntityConfiguration = {
	type: SimplePolygon.TYPE,
	entity: SimplePolygon,
	shape2D: SimplePolygonShape2D,
	shape3D: SimplePolygonShape3D,
	handle2D: SimplePolygonHandle2D
};
