import {
	EntityShapeMesh,
	EntityShape2D,
	Material2D,
	Stage,
	View2D
}                               from "sdlib";
import {SimplePolygon}          from "./SimplePolygon";

export class SimplePolygonShape2D extends EntityShape2D<SimplePolygon>
{
	private _material2D: Material2D;
	private readonly _mesh: EntityShapeMesh;

	public constructor(entity: SimplePolygon, stage: Stage<View2D>)
	{
		super(entity, stage);
		this._container = this.adapter.factory.createObject3D();

		// create the note background shape as a threejs mesh
		this.stage.add(this._container);

		this._mesh = new EntityShapeMesh(stage.view.adapter, this._container);
	}

	private updateMaterial(): void
	{
		if (this._material2D)
		{
			this._material2D.dispose();
		}

		// create material, note: this could be cached, to avoid constant creation
		this._material2D = this.createMaterial2D(this._entity.material);

		// set material opacity
		this._material2D.update({
			mixColor: this._entity.material.color,
			mixColorOpacity: this._entity.material.opacity / 100,
			transparent: true,
			ignoreInObjectIdMap: false,
			opacity: 1
		});
	}

	private createMesh(): void
	{
		const corners = [];
		for (const point of this._entity.corners)
		{
			corners.push(this.adapter.factory.createVector2(point.x, point.y));
		}

		const shape = this.adapter.factory.createShape(corners);
		const geometry = this.adapter.factory.createShapeGeometry(shape);

		// create mesh with the given material and geometry
		this._mesh.redraw(geometry, this._material2D);
	}

	protected updateMesh(): void
	{
		this._container.rotation.x = Math.PI / 2;

		this._mesh.clear();

		this.updateMaterial();
		this.createMesh();
	}

	public redraw(): void
	{
		super.redraw();

		// draw entity if visible
		if (this._entity.isActive)
		{
			this.updateMesh();
		}
	}

	public dispose(): void
	{
		this._mesh.dispose();
		// when surface gets dispose before it drawn then the material2D does not exist
		if (this._material2D)
		{
			this._material2D.dispose();
		}

		super.dispose();
	}
}
