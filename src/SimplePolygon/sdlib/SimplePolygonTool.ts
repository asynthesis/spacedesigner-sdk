import {
	Stage,
	View2D,
	EntityTool,
	IEntityToolParams,
	IMaterialData,
	PolylineTool,
	PolylineToolMouse,
	PolylineToolTouch
}                                            from "sdlib";
import {ISimplePolygonData, SimplePolygon}   from "./SimplePolygon";
import {ISimplePolygonCornerData}      from "./SimplePolygonCorner";

export interface ISimplePolygonToolParams extends IEntityToolParams
{
	height?: number;
	thickness?: number;
	material?: IMaterialData;
}

export class SimplePolygonTool extends EntityTool
{
	protected _polylineTool: PolylineTool;

	protected _material: IMaterialData = {id: 1};

	public constructor(polylineTool: PolylineTool)
	{
		super();
		this._polylineTool = polylineTool;
	}

	public initWithView(stage: Stage<View2D>, params: ISimplePolygonToolParams = {}): void
	{
		this._material = params.material;

		this._stage = stage;
		this._scene = stage.view.scene;

		this._polylineTool.initWithView(stage);
		this._polylineTool.polylineSignals.doubleClicked.add(this.onDoubleClicked, this);
		this._polylineTool.polylineSignals.completed.add(this.onCompleted, this);
		this._polylineTool.polylineSignals.disposed.add(this.dispose, this);
	}

	private onDoubleClicked(): void
	{
		this._polylineTool.finalize();
		this.dispose();
	}

	protected onCompleted(): void
	{
		const paths = this._polylineTool.paths;

		for (const path of paths)
		{
			const corners: ISimplePolygonCornerData[] = [];

			for (const corner of path)
			{
				const cornerData: ISimplePolygonCornerData = {
					x: corner.X,
					y: corner.Y,
					// surface will be assigned in SimplePolygon entity constructor
					simplePolygon: null
				};
				corners.push(cornerData);
			}

			const simplePolygonData: ISimplePolygonData = {
				type: SimplePolygon.TYPE,
				level: this._scene.levels.active.uid,
				version: this._scene.versions.active.uid,
				points: corners,
				material: this._material
			};

			SimplePolygon.Init(this._scene, simplePolygonData);
		}

		// end interaction
		this.dispose();
	}

	public dispose(): void
	{
		super.dispose();
		this._polylineTool.disposeCore();
	}
}

export class SimplePolygonToolTouch extends SimplePolygonTool
{
	public constructor()
	{
		super(new PolylineToolTouch());
	}
}

export class SimplePolygonToolMouse extends SimplePolygonTool
{
	public constructor()
	{
		super(new PolylineToolMouse(1));
	}

	public redraw(): void
	{
		this._polylineTool.redraw();
	}
}

