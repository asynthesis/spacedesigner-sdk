import {
	Box3D,
	Transparency,
	IMaterial,
	Vector2,
	IMesh,
	Stage,
	EntityShape3D,
	View3D
}                      from "sdlib";
import {SimplePolygon} from "../sdlib/SimplePolygon";

export class SimplePolygonShape3D extends EntityShape3D<SimplePolygon>
{
	protected _mesh: IMesh;

	private _material: IMaterial;

	public constructor(entity: SimplePolygon, stage: Stage<View3D>)
	{
		super(entity, stage);

		this._container = this.adapter.factory.createObject3D();
		this.stage.add(this._container);
	}

	public redraw(): void
	{
		super.redraw();
	}

	protected updateMesh(): void
	{
		if (this._material)
		{
			this._material.dispose();
		}
		this._material = this.createMaterial3D(this._entity.material, Transparency.Opaque, 0xcccccc);

		if (this._mesh)
		{
			this.container.remove(this._mesh);
			this._mesh.geometry.dispose();
		}

		const corners: Vector2[] = [];
		for (const corner of this.entity.corners)
		{
			corners.push(this.adapter.factory.createVector2(corner.x, corner.y));
		}

		if (!corners.length)
		{
			return;
		}

		const shape = this.adapter.factory.createShape(corners);

		// create an extruded polygon based shape, which height is 10
		const extrudeSettings = {
			depth: 10,
			steps: 1,
			bevelEnabled: false
		};
		const geometry = this.adapter.factory.createExtrudeGeometry(shape, extrudeSettings);

		this._mesh = this.adapter.factory.createMesh(geometry, this._material);
		this._mesh.rotation.x = Math.PI / 2;

		this._container.add(this._mesh);

		this.updateShadows(true, true);
	}

	public getBoundingBox3D(): Box3D
	{
		return this._entity.getBoundingBox3D();
	}

	public dispose(): void
	{
		if (this._mesh)
		{
			this.container.remove(this._mesh);
			this._mesh.geometry.dispose();
		}
		if (this._material)
		{
			this._material.dispose();
		}
		super.dispose();
	}
}
