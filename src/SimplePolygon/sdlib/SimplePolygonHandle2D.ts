import {
	DomSprite,
	DragHandle,
	Pointer,
	Stage,
	View2D
}                            from "sdlib";
import {SimplePolygon}       from "./SimplePolygon";
import {SimplePolygonCorner} from "./SimplePolygonCorner";

export class SimplePolygonHandle2D extends DragHandle<SimplePolygon, View2D>
{
	private _shapePivotSign: DomSprite;
	private _corners: DomSprite[] = [];
	private _activeCorner: SimplePolygonCorner;

	public constructor(entity: SimplePolygon, stage: Stage<View2D>, pointer?: Pointer)
	{
		super(entity, stage, pointer);

		this.createPivotSign();
		this.updateCorners();

		this.view.signals.scaleChanged.add(this.scaleChanged, this);
		this.scaleChanged();
	}

	protected createPivotSign(): void
	{
		this._shapePivotSign = this.adapter.factory.createDomSprite(`<div class="pivotSign"></div>`);
		this.view.dom.add(this._shapePivotSign);
	}

	private scaleChanged(): void
	{
		// handle zoom changes
		const scale = 1 / this.view.scale;
		this._shapePivotSign.setScale(scale);
		for (let i = 0; i < this._corners.length; i++)
		{
			this._corners[i].setPos(this._entity.corners[i].x, this._entity.corners[i].y);
			this._corners[i].setScale(scale);
		}
	}

	private readonly updateCorners = (): void =>
	{
		for (const corner of this._corners)
		{
			corner.dispose();
		}
		this._corners = [];

		// in viewer or if the entity is locked, it should not show the corners
		if (this.view.scene.mainParams.editable && !this.entity.locked)
		{
			this._entity.corners.forEach((corner: SimplePolygonCorner, i: number) =>
			{
				const newSprite = this.adapter.factory.createDomSprite(`<div class="simplePolygonCornerShape"></div>`);
				this.view.dom.add(newSprite);
				this._corners.push(newSprite);
				newSprite.signals.down.add((pointer: Pointer) => this.onCornerMouseDown(pointer, i));
				newSprite.signals.up.add(() => this.onCornerMouseUp(i));
			});
			this.scaleChanged();
		}	
	};

	private onCornerMouseDown(pointer: Pointer, cornerIndex: number): void
	{
		// a corner has been clicked, set it the active corner
		this._activeCorner = this._entity.corners[cornerIndex];
		this._corners[cornerIndex].signals.move.add(this.onCornerMouseMove, this);
		pointer.originalEvent.stopImmediatePropagation();
	}

	private onCornerMouseMove(pointer: Pointer): void
	{
		// active corner mouse move handling
		const prev = this._entity.getPrevCorner(this._activeCorner);
		const next = this._entity.getNextCorner(this._activeCorner);

		const snapped = this.view.snapSpace.trySnappingPointer([prev.p, next.p], pointer, {
			ignoreEntityUids: [this._entity],
			findOrtho: true
		});

		this._activeCorner.moveTo(snapped);
		this._entity.invalidate();
	}

	private onCornerMouseUp(cornerIndex: number): void
	{
		// end corner interaction
		this._activeCorner = null;
		this._corners[cornerIndex].signals.move.remove(this.onCornerMouseMove, this);
	}

	public redraw(): void
	{
		if (this._entity.invalidationCount !== this._invalidationCount)
		{
			super.redraw();

			// update the central pivot sign position
			this._shapePivotSign.setPos(this._entity.x, this._entity.y);

			// update corner positions
			for (let i = 0; i < this._corners.length; i++)
			{
				this._corners[i].setPos(this._entity.corners[i].x, this._entity.corners[i].y);
			}
		}
	}

	public dispose(): void
	{
		this.view.signals.scaleChanged.remove(this.scaleChanged, this);
		this._shapePivotSign.dispose();
		for (const corner of this._corners)
		{
			corner.dispose();
		}

		super.dispose();
	}
}
