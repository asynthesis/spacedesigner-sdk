import * as React             from 'react';
import {C}                    from "sdlib";
import {Button, EditorMain}   from "spacedesigner";
import {SimplePrimitiveForm}  from "../sdlib/SimplePrimitive";
import {SimplePrimitiveTool}  from "../sdlib/SimplePrimitiveTool";

export interface ISimplePrimitiveTabProps
{
}

export interface ISimplePrimitiveTabStates
{
}

export class SimplePrimitiveTab extends React.PureComponent<ISimplePrimitiveTabProps, ISimplePrimitiveTabStates>
{
	private readonly onBoxButtonClick = (): void =>
	{
		EditorMain.instance.getView().initTool(SimplePrimitiveTool, { form: SimplePrimitiveForm.BOX });
	};

	private readonly onCylinderButtonClick = (): void =>
	{
		EditorMain.instance.getView().initTool(SimplePrimitiveTool, { form: SimplePrimitiveForm.CYLINDER });
	};

	private readonly onSphereButtonClick = (): void =>
	{
		EditorMain.instance.getView().initTool(SimplePrimitiveTool, { form: SimplePrimitiveForm.SPHERE });
	};

	public render(): JSX.Element
	{
		return (
			<div className="sd-simpleprimitive-tab">
				<div className="leftpanel-padding">
					<span className="title1">{C('simpleprimitive tab')}</span>
					<br/>
					<Button
						cls="secondary"
						label={C("create box")}
						onInteract={this.onBoxButtonClick}
					/>
					<Button
						cls="secondary"
						label={C("create cylinder")}
						onInteract={this.onCylinderButtonClick}
					/>
					<Button
						cls="secondary"
						label={C("create sphere")}
						onInteract={this.onSphereButtonClick}
					/>
				</div>
			</div>
		);
	}
}
