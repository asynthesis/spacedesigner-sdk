import * as React                        from "react";
import {C}                               from "sdlib";
import {PropanelContainer}               from "spacedesigner";
import {
	SimplePrimitive,
	SimplePrimitiveForm
}                                        from "../sdlib/SimplePrimitive";
import {SimplePrimitivePanelDimensions}  from "./SimplePrimitivePanelDimensions";
import {SimplePrimitivePanelMaterials}   from "./SimplePrimitivePanelMaterials";

interface ISimplePrimitivePanelProps
{
	entity: SimplePrimitive;
}

export class SimplePrimitivePanel extends React.Component<ISimplePrimitivePanelProps, {}>
{
	private readonly dimensions = React.createRef<SimplePrimitivePanelDimensions>();
	private readonly materials = React.createRef<SimplePrimitivePanelMaterials>();

	public constructor(props: ISimplePrimitivePanelProps)
	{
		super(props);
	}

	public get label(): string
	{
		if (this.props.entity.form === SimplePrimitiveForm.CYLINDER)
		{
			return C('cylinder');
		}
		else if (this.props.entity.form === SimplePrimitiveForm.SPHERE)
		{
			return C('sphere');
		}
		return C('box');
	}

	private readonly conditionalUpdate = (): void =>
	{
		if (this.dimensions && this.dimensions.current) this.dimensions.current.conditionalUpdate();
	};

	public render(): JSX.Element
	{
		const { entity } = this.props;

		return (
			<PropanelContainer
				className="sd-abstractpanel sd-simpleprimitivepanel"
				entity={entity}
				updateFn={this.conditionalUpdate}
				enableDuplicate={true}
				enableDelete={true}
			>
				<div className="primitive-panel-wrapper">
					<div className="title">{this.label}</div>
					<SimplePrimitivePanelDimensions entity={entity} ref={this.dimensions}/>
				</div>
				<SimplePrimitivePanelMaterials entity={entity} ref={this.materials}/>
			</PropanelContainer>
		);
	}
}
