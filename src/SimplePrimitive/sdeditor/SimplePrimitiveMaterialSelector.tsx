import * as React                       from "react";
import {
	Assets,
	C,
	FileDropper,
	ImageUtil,
	IImageUtilDone,
	Material
}                                       from "sdlib";
import {
	Button,
	Label,
	ISelectOption,
	Select,
	MaterialDialog,
	FileDropperOverlay,
	NumericStepper,
	MaterialRenderer,
	EditorMain
}                                       from "spacedesigner";

import {SimplePrimitive}                from "../sdlib/SimplePrimitive";
import {SimplePrimitiveMaterialType}    from "../sdlib/SimplePrimitiveMaterial";

interface ISimplePrimitiveMaterialSelectorProps
{
	entity: SimplePrimitive;
	index: number;
}

interface ISimplePrimitiveMaterialSelectorState
{
	material: Material;
	type: SimplePrimitiveMaterialType;
	imageKey: string,
	thumbnailKey: string,
	zoom: number
}

export class SimplePrimitiveMaterialSelector extends React.PureComponent<ISimplePrimitiveMaterialSelectorProps, ISimplePrimitiveMaterialSelectorState>
{
	private readonly pictureInput = React.createRef<HTMLInputElement>();

	public constructor(props: ISimplePrimitiveMaterialSelectorProps)
	{
		super(props);
		const material = props.entity.materials[props.index];
		this.state = {
			material: material.material,
			type: material.type,
			imageKey: material.imageKey,
			thumbnailKey: material.thumbnailKey,
			zoom: material.zoom
		};
	}


	private get typeOptions(): ISelectOption[]
	{
		// the two options for the primitive material type
		const options: ISelectOption[] = [];
		options.push({
			value: SimplePrimitiveMaterialType.MATERIAL,
			name: C('material'),
			selected: this.state.type === SimplePrimitiveMaterialType.MATERIAL
		});
		options.push({
			value: SimplePrimitiveMaterialType.PICTURE,
			name: C('picture'),
			selected: this.state.type === SimplePrimitiveMaterialType.PICTURE
		});

		return options;
	}

	private readonly onTypeChanged = (e: React.ChangeEvent<HTMLSelectElement>): void =>
	{
		// handle material type changes
		const {entity, index} = this.props;
		entity.setMaterialType(e.target.value as SimplePrimitiveMaterialType, this.props.index);
		this.setState({ type: entity.materials[index].type });
	};

	private readonly onMaterialClicked = (): void =>
	{
		// apply selected material from material dialog
		const {entity, index} = this.props;
		EditorMain.instance.showDialogBox(<MaterialDialog
			material={this.state.material}
			applyCallback={(material) =>
			{
				entity.setMaterial(material, index);
				this.setState({ material: entity.materials[index].material });
			}}
		/>);
	};

	private readonly onMaterialSampleClicked = (value: Material): void =>
	{
		// apply sampled material from the scene
		const {entity, index} = this.props;
		entity.setMaterial(value, index);
		this.setState({ material: entity.materials[index].material });
	};

	private readonly onHover = (): void =>
	{
		// highlight the primitive side in 3d when hovered on this material selector
		this.props.entity.highlightIndex = this.props.index;
	};

	private readonly onMouseOut = (): void =>
	{
		// disabled the highlight on the primitive side
		this.props.entity.highlightIndex = -1;
	};

	private readonly onUploadClicked = (): void =>
	{
		if (this.pictureInput && this.pictureInput.current)
		{
			this.pictureInput.current.click();
		}
	};

	private readonly onZoomChanged = (value:number): void =>
	{
		// apply the texture scale factor for uploaded image
		const {entity, index} = this.props;
		entity.setPictureZoom(value, index);
		this.setState({ zoom: entity.materials[index].zoom });
	};

	private readonly addPicture = (e: React.ChangeEvent<HTMLInputElement>): void =>
	{
		// upload image to the uploads folder
		const {entity, index} = this.props;
		const file = Array.from(e.target.files).filter(FileDropper.isImageFile)[0];

		if (file)
		{
			ImageUtil.upload({
				apiRoot: EditorMain.instance.params.apiRoot,
				imageFile: file,
				imageMaxSize: {width: 1024, height: 1024},
				createThumb: true,
				thumbMaxSize: {width: 170, height: 170},
				onComplete: (data: IImageUtilDone) =>
				{
					// image filename on successful upload
					let imageKey = data.imageKey;

					// thumbnail filename on successful upload
					let thumbnailKey = data.thumbnailKey;

					// handle some edge case
					if (!data.imageKey)
					{
						const key = (data as any).path.split('/').pop();
						imageKey = key;
						thumbnailKey = key;
					}

					// update the primitive entity with the custom filenames
					entity.setPictureKeys(imageKey, thumbnailKey, index);

					// update the gui as well
					this.setState({ imageKey, thumbnailKey });
				},
				onFail: (error) => EditorMain.instance.showDialogBox(<FileDropperOverlay message={C(error.desc)} cancelable={true}/>)
			});
		}
	};

	public render(): JSX.Element
	{
		const {imageKey, thumbnailKey} = this.state;
		let previewPath = "";
		if (thumbnailKey)
		{
			previewPath = Assets.getUrl('uploads/images/' + thumbnailKey);
		}
		else if (imageKey)
		{
			previewPath = Assets.getUrl('uploads/images/' + imageKey);
		}

		return <>
			<div className="hbox">
				<Label className="subtitle" text={C('type')}/>
				<Select options={this.typeOptions} onChange={this.onTypeChanged}/>
			</div>
			{
				this.state.type === SimplePrimitiveMaterialType.MATERIAL &&
				<div className="hbox">
					<Label text={C('material')}/>
					<MaterialRenderer
						material={this.state.material}
						onClick={this.onMaterialClicked}
						onSampleClick={this.onMaterialSampleClicked}
						onHover={this.onHover}
						onMouseOut={this.onMouseOut}
					/>
				</div>
			}
			{
				this.state.type === SimplePrimitiveMaterialType.PICTURE &&
				<>
					{
						previewPath &&
						<div className="hbox previewWrapper" onClick={this.onUploadClicked} onMouseOver={this.onHover} onMouseOut={this.onMouseOut}>
							<img
								src={previewPath}
								className="previewPicture"
							/>
						</div>
					}
					{
						previewPath &&
						<div className="hbox">
							<Label text={C('zoom')}/>
							<NumericStepper
								sign="%"
								min={1}
								stepSize={1}
								onChange={this.onZoomChanged}
								value={this.state.zoom}
								assetsUrl={EditorMain.instance.params.url_ui_assets}
							/>
						</div>
					}
					<Button
						cls="secondary button"
						onInteract={this.onUploadClicked}
						label={C('upload new picture')}
					/>
					<input
						ref={this.pictureInput}
						style={{display: "none"}}
						id="skyboxInput"
						type="file"
						name="file"
						accept="image/x-png,image/jpeg"
						onChange={this.addPicture}
					/>
				</>
			}
		</>;
	}
}
