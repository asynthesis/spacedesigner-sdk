import * as React       from "react";
import {
	C
}                       from "sdlib";
import {
	Label,
	EditorMain,
	DimInput,
	NumericStepper
}                       from "spacedesigner";
import {
	SimplePrimitive,
	SimplePrimitiveForm
}                       from "../sdlib/SimplePrimitive";

interface ISimplePrimitivePanelProps
{
	entity: SimplePrimitive;
}

interface ISimplePrimitivePanelDimensionsState
{
	z: number;
	width: number;
	height: number;
	length: number;
	rotation: number;
}

export class SimplePrimitivePanelDimensions extends React.PureComponent<ISimplePrimitivePanelProps, ISimplePrimitivePanelDimensionsState>
{
	public constructor(props: ISimplePrimitivePanelProps)
	{
		super(props);
		const entity = props.entity;

		let width = entity.width;
		if (entity.form === SimplePrimitiveForm.CYLINDER || entity.form === SimplePrimitiveForm.SPHERE)
		{
			width /= 2;
		}
		this.state = {
			z: entity.z,
			width: width,
			height: entity.height,
			length: entity.length,
			rotation: entity.rotation
		};
	}

	public get widthLabel(): string
	{
		const entity = this.props.entity;
		if (entity.form === SimplePrimitiveForm.CYLINDER || entity.form === SimplePrimitiveForm.SPHERE)
		{
			return C('radius');
		}
		return C('width');
	}

	public conditionalUpdate = (): void =>
	{
		const entity = this.props.entity;
		if (
			this.state.z !== entity.z ||
			this.state.width !== entity.width ||
			this.state.height !== entity.height ||
			this.state.length !== entity.length ||
			this.state.rotation !== entity.rotation
		)
		{
			let width = this.props.entity.width;
			if (entity.form === SimplePrimitiveForm.CYLINDER || entity.form === SimplePrimitiveForm.SPHERE)
			{
				width /= 2;
			}
			this.setState({
				z: entity.z,
				width: width,
				height: entity.height,
				length: entity.length,
				rotation: entity.rotation
			});
		}
	};

	private readonly onWidthChanged = (value: number): void =>
	{
		const entity = this.props.entity;
		if (entity.form === SimplePrimitiveForm.CYLINDER || entity.form === SimplePrimitiveForm.SPHERE)
		{
			entity.width = value * 2;
			entity.length = entity.width;
			if (entity.form === SimplePrimitiveForm.SPHERE)
			{
				entity.height = entity.width;
			}
		}
		else
		{
			entity.width = value;
		}
	};

	private readonly onHeightChanged = (value: number): void =>
	{
		this.props.entity.height = value;
	};

	private readonly onLengthChanged = (value: number): void =>
	{
		this.props.entity.length = value;
	};

	private readonly onRaiseChanged = (value: number): void =>
	{
		this.props.entity.z = value;
	};

	private readonly onRotationChanged = (value: number): void =>
	{
		this.props.entity.rotation = value;
	};

	public render(): JSX.Element
	{
		const {entity} = this.props;
		const {width, length, height, z, rotation} = this.state;
		const params = EditorMain.instance.params;

		return (
			<>
				<div className="proportions-box">
					<div className="properties-wrapper">
						<div className="hbox">
							<Label text={this.widthLabel}/>
							<DimInput
								min={0}
								onChange={this.onWidthChanged}
								value={width}
							/>
						</div>
						{
							(entity.form === SimplePrimitiveForm.CYLINDER || entity.form === SimplePrimitiveForm.BOX) &&
							<div className="hbox">
								<Label text={C('height')}/>
								<DimInput
									min={0}
									onChange={this.onHeightChanged}
									value={height}
								/>
							</div>
						}
						{
							entity.form === SimplePrimitiveForm.BOX &&
							<div className="hbox">
								<Label text={C('length')}/>
								<DimInput
									min={0}
									onChange={this.onLengthChanged}
									value={length}
								/>
							</div>
						}
						<div className="hbox">
							<Label text={C('distance')}/>
							<DimInput
								min={0}
								onChange={this.onRaiseChanged}
								value={z}
							/>
						</div>
						<div className="hbox">
							<Label text={C('rotation')}/>
							<NumericStepper
								sign="°"
								value={rotation}
								onChange={this.onRotationChanged}
								assetsUrl={params.url_ui_assets}
							/>
						</div>
					</div>
				</div>
			</>
		);
	}
}
