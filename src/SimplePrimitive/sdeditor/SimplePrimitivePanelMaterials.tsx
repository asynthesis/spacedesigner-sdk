import * as React                              from "react";
import {
	C
}                                              from "sdlib";
import {PropanelAccordion}                     from "spacedesigner";
import {SimplePrimitiveMaterialSelector}       from "./SimplePrimitiveMaterialSelector";
import {SimplePrimitive, SimplePrimitiveForm}  from "../sdlib/SimplePrimitive";
import {SimplePrimitiveMaterial}               from "../sdlib/SimplePrimitiveMaterial";


interface ISimplePrimitivePanelMaterialsProps
{
	entity: SimplePrimitive;
}

interface ISimplePrimitivePanelMaterialsState
{
	materials: SimplePrimitiveMaterial[]
}

export class SimplePrimitivePanelMaterials extends React.PureComponent<ISimplePrimitivePanelMaterialsProps, ISimplePrimitivePanelMaterialsState>
{
	public constructor(props: ISimplePrimitivePanelMaterialsProps)
	{
		super(props);
		this.state = {
			materials: [...props.entity.materials]
		};
	}

	public renderBoxMaterials(): JSX.Element
	{
		return <>
			<PropanelAccordion title={C('right material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={0}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
			<PropanelAccordion title={C('left material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={1}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
			<PropanelAccordion title={C('top material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={2}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
			<PropanelAccordion title={C('bottom material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={3}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
			<PropanelAccordion title={C('front material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={4}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
			<PropanelAccordion title={C('back material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={5}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
		</>;
	}

	public renderCylinderMaterials(): JSX.Element
	{
		return <>
			<PropanelAccordion title={C('side material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={0}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
			<PropanelAccordion title={C('top material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={1}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
			<PropanelAccordion title={C('bottom material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={2}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
		</>;
	}

	public renderSphereMaterials(): JSX.Element
	{
		return <>
			<PropanelAccordion title={C('material')}>
				<SimplePrimitiveMaterialSelector entity={this.props.entity} index={0}></SimplePrimitiveMaterialSelector>
			</PropanelAccordion>
		</>;
	}

	public render(): JSX.Element
	{
		const {entity} = this.props;
		return <>
			{
				entity.form === SimplePrimitiveForm.BOX && this.renderBoxMaterials()
			}
			{
				entity.form === SimplePrimitiveForm.CYLINDER && this.renderCylinderMaterials()
			}
			{
				entity.form === SimplePrimitiveForm.SPHERE && this.renderSphereMaterials()
			}
		</>;
	}
}
