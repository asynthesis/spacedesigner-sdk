import {
	Assets,
	EntityShape2D,
	GeomUtils,
	IMaterial2DConfig,
	IMesh,
	Material2D,
	Stage,
	View2D,
	Wrapping
}                                       from "sdlib";
import {
	SimplePrimitive,
	SimplePrimitiveForm
}                                       from "./SimplePrimitive";
import {SimplePrimitiveMaterialType}    from "./SimplePrimitiveMaterial";
import {SimplePrimitiveShapeBuilder}    from "./SimplePrimitiveShapeBuilder";

export class SimplePrimitiveShape2D extends EntityShape2D<SimplePrimitive>
{
	private mesh: IMesh;
	private readonly shapeBuilder: SimplePrimitiveShapeBuilder;

	private material: {
		// signature used for caching purposes, avoid recreation multiple times
		signature: string,
		// threejs material
		threeMaterial: Material2D
	};

	// cached dimension values of the primitive
	private cached: {
		width: number,
		height: number,
		length: number
	};

	public constructor(entity: SimplePrimitive, stage: Stage<View2D>)
	{
		super(entity, stage);

		this._container = this.adapter.factory.createObject3D();
		this.stage.add(this._container);

		// create the proper shape builder object
		this.shapeBuilder = SimplePrimitiveShapeBuilder.getShapeBuilder(this.adapter, entity);

		// create the initial materials
		this.createMaterial();
		
		// create the initial mesh
		this.createMesh();
		
		// store the initial cache
		this.cacheEntity();

		// rotate container to match the camera view
		this._container.rotation.x = -Math.PI / 2;
	}

	public redraw(): void
	{
		this.updateMesh();
		super.redraw();
	}

	protected getMaterialIndex(): number
	{
		// defines which material to use from the materials array for the top view representation in view 2D
		return this.entity.form === SimplePrimitiveForm.BOX ? 2 : this.entity.form === SimplePrimitiveForm.CYLINDER ? 1 : 0;
	}

	protected cacheEntity(): void
	{
		this.cached = {
			width: this.entity.width,
			height: this.entity.height,
			length: this.entity.length
		};
	}

	protected createMesh(): void
	{
		if (this.mesh)
		{
			this._container.remove(this.mesh);
			this.mesh.dispose();
		}

		this.mesh = this.shapeBuilder.createMesh2D(this.material.threeMaterial);

		this._container.add(this.mesh);
	}

	protected createMaterial(): void
	{
		// dispose the previous material
		if (this.material)
		{
			this.material.threeMaterial.dispose();
		}

		const index = this.getMaterialIndex();

		const pm = this.entity.materials[index];

		if (pm.type === SimplePrimitiveMaterialType.MATERIAL)
		{
			// create a built in material using EntityShape2D createMaterial2D, based on sd material id
			const material = pm.material;
			this.material = {
				signature: pm.signature,
				threeMaterial: this.createMaterial2D(material)
			};
			this.material.threeMaterial.update({
				mixColor: material.color,
				mixColorOpacity: material.opacity / 100
			});
		}
		else if (pm.type === SimplePrimitiveMaterialType.PICTURE)
		{
			// create a material, based on an uploaded image
			if (pm.imageKey)
			{
				// image is uploaded here
				const textureUrl = Assets.getUrl("uploads/images/" + pm.imageKey);

				// create material, use the texture scale factor with negative values, repeat is on
				const materialOptions: IMaterial2DConfig = {
					textureUrl: textureUrl,
					textureScale: -100 / pm.zoom,
					textureWrap: Wrapping.RepeatWrapping,
					onTextureLoaded: this.onPictureTextureLoaded,
					onTextureLoadError: this.onPictureTextureFailed,
					adapter: this._adapter
				};

				// use the adapter to create the threejs material used in 2D view
				this.material = {
					signature: pm.signature,
					threeMaterial: this.adapter.factory.createMaterial2D({ ...materialOptions, adapter: this.adapter })
				};
			}
			else
			{
				// default white material, the image is not uploaded/selected yet
				const materialOptions: IMaterial2DConfig = {
					mixColor: 0xffffff, 
					adapter: this._adapter
				};
				this.material = {
					signature: pm.signature,
					threeMaterial: this.adapter.factory.createMaterial2D({ ...materialOptions, adapter: this.adapter })
				};
			}
		}

		// update the material parameter of the mesh
		if (this.mesh)
		{
			this.mesh.material = this.material.threeMaterial;
		}
	}

	protected updateMesh(): void
	{
		// update the materials only if the materials have changed
		if (this.hasMaterialChanged())
		{
			this.createMaterial();
			this.cacheEntity();
		}

		// update the mesh only if the dimensions have changed
		if (this.hasDimensionChanged())
		{
			this.createMesh();
			this.cacheEntity();
		}

		// build the proper shape for 2d view
		this.shapeBuilder.updateMesh2D(this.mesh);

		// update positions and rotation
		this._container.position.set(this.entity.x, this.entity.z, this.entity.y);
		this._container.rotation.set(-Math.PI / 2, 0, -this.entity.rotation * GeomUtils.D2R);
	}

	private readonly onPictureTextureLoaded = (): void =>
	{
		this.entity.invalidate();
	};

	private readonly onPictureTextureFailed = (errorMessage: string): void =>
	{
		console.warn("SimplePrimitive picture texture failed to load: ", errorMessage);
	};

	private hasMaterialChanged(): boolean
	{
		// the top view material has changed if the signatures are not matching in the entity and in the shape
		return this.entity.materials[this.getMaterialIndex()].signature !== this.material.signature;
	}

	private hasDimensionChanged(): boolean
	{
		// dimensions changed
		const dimensionsChanged = 
			this.cached.width !== this.entity.width ||
			this.cached.height !== this.entity.height ||
			this.cached.length !== this.entity.length;

		return dimensionsChanged;
	}

	public dispose(): void
	{
		this.material = null;
		this._container.remove(this.mesh);
		this.mesh.dispose();
		super.dispose();
	}
}
