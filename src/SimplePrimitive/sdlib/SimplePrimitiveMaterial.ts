import {IMaterialData, Material}      from "sdlib";

export enum SimplePrimitiveMaterialType
	{
	MATERIAL='material',
	PICTURE='picture'
}

// raw serialized data of the primitive material
export interface ISimplePrimitiveMaterialData
{
	type: SimplePrimitiveMaterialType,
	materialData: IMaterialData,
	imageKey: string,
	thumbnailKey: string,
	zoom: number
}

export class SimplePrimitiveMaterial
{
	// primitive material type can be an uploaded picture or a built-in material
	private _type: SimplePrimitiveMaterialType;

	// the built-in material
	private _material: Material;

	// the uploaded image filename
	private _imageKey: string;

	// the uploaded image thumbnail filename
	private _thumbnailKey: string;

	// the scale of the uploaded image
	private _zoom: number;

	public constructor(data: ISimplePrimitiveMaterialData)
	{
		this._type = data.type;
		this._material = new Material(data.materialData);
		this._imageKey = data.imageKey;
		this._thumbnailKey = data.thumbnailKey;
		this._zoom = data.zoom;
	}

	public get type(): SimplePrimitiveMaterialType
	{
		return this._type;
	}

	public set type(value: SimplePrimitiveMaterialType)
	{
		this._type = value;
	}

	public get material(): Material
	{
		return this._material;
	}

	public set material(material: Material)
	{
		this._material = material;
	}

	public get imageKey(): string
	{
		return this._imageKey;
	}

	public set imageKey(value: string)
	{
		this._imageKey = value;
	}

	public get thumbnailKey(): string
	{
		return this._thumbnailKey;
	}

	public set thumbnailKey(value: string)
	{
		this._thumbnailKey = value;
	}

	public get zoom(): number
	{
		return this._zoom;
	}

	public set zoom(value: number)
	{
		this._zoom = value;
	}

	public get signature(): string
	{
		// signature used to cache the materials to avoid unnecessary recreation
		if (this._type === SimplePrimitiveMaterialType.MATERIAL)
		{
			return this.material.signature;
		}
		else if (this._type === SimplePrimitiveMaterialType.PICTURE)
		{
			return this._imageKey + this._zoom;
		}
	}
}