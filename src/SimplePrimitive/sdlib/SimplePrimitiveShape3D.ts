import {
	Assets,
	Color,
	EntityShape3D,
	GeomUtils,
	IMaterial,
	IMesh,
	MeshStandardMaterial,
	Side,
	Stage,
	Transparency,
	View3D,
	Wrapping
}                                     from "sdlib";
import {
	SimplePrimitive,
	SimplePrimitiveForm
}                                     from "./SimplePrimitive";
import {SimplePrimitiveShapeBuilder}  from "./SimplePrimitiveShapeBuilder";
import {SimplePrimitiveMaterialType}  from "./SimplePrimitiveMaterial";

export class SimplePrimitiveShape3D extends EntityShape3D<SimplePrimitive>
{
	private mesh: IMesh;
	private readonly shapeBuilder: SimplePrimitiveShapeBuilder;
	private  materials: {
		// signature used for caching purposes, avoid recreation multiple times
		signature: string,
		// threejs material
		threeMaterial: MeshStandardMaterial
	} [] = [];

	// cached dimension and highlight values of the primitive
	private cached: {
		width: number,
		height: number,
		length: number,
		highlightIndex: number
	};

	public constructor(entity: SimplePrimitive, stage: Stage<View3D>)
	{
		super(entity, stage);
		this._container = this.adapter.factory.createObject3D();
		this.stage.add(this._container);

		// create the proper shape builder object
		this.shapeBuilder = SimplePrimitiveShapeBuilder.getShapeBuilder(this.adapter, this.entity);

		// create the initial materials
		this.createMaterials();

		// create the initial mesh
		this.createMesh();

		// store the initial cache
		this.cacheEntity();
	}

	private readonly onPictureTextureLoaded = (): void =>
	{
		this.entity.invalidate();
	};

	private readonly onPictureTextureFailed = (errorMessage: string): void =>
	{
		console.warn("SimplePrimitive picture texture failed to load: ", errorMessage);
	};

	private createMesh(): void
	{
		// dispose old mesh
		if (this.mesh)
		{
			this._container.remove(this.mesh);
			this.mesh.dispose();
		}

		// create the 3D mesh with an array of materials
		this.mesh = this.shapeBuilder.createMesh3D(this.materials.map((material): MeshStandardMaterial =>
		{
			return material.threeMaterial; 
		}));

		// turn on shadows
		this.mesh.castShadow = true;
		this.mesh.receiveShadow = true;

		// add mesh to the container object
		this._container.add(this.mesh);
	}

	private createMaterials(): void
	{
		for (const material of this.materials)
		{
			material.threeMaterial.dispose();
		}
		this.materials = [];

		for (const [index, pm] of this.entity.materials.entries())
		{
			// create a built in material using EntityShape3D createMaterial3D, based on sd material id
			let threeMaterial: MeshStandardMaterial;
			if (pm.type === SimplePrimitiveMaterialType.MATERIAL)
			{
				threeMaterial = this.createMaterial3D(pm.material, Transparency.Opaque);
				threeMaterial.side = Side.DoubleSide;
			}
			else if (pm.type === SimplePrimitiveMaterialType.PICTURE)
			{
				// create a material, based on an uploaded image
				if (pm.imageKey)
				{
					// this is where the uploaded images are stored
					const textureUrl = Assets.getUrl("uploads/images/" + pm.imageKey);

					// use the texture scale factor with negative values
					const texture = this.adapter.factory.loadTexture(textureUrl, [-pm.zoom / 100, -pm.zoom / 100], this.onPictureTextureLoaded, this.onPictureTextureFailed);

					// repeat is on
					texture.wrapS = Wrapping.RepeatWrapping;
					texture.wrapT = Wrapping.RepeatWrapping;

					// use the adapter to create the threejs material used in 3D view
					threeMaterial = this.adapter.factory.createMeshStandardMaterial({
						map: texture,
						transparent: true,
						side: Side.DoubleSide
					});
				}
				else
				{
					// default white material, the image is not uploaded/selected yet
					threeMaterial = this.adapter.factory.createMeshStandardMaterial({
						color: 0xffffff,
						transparent: true,
						side: Side.DoubleSide
					});
				}
			}

			if (index === this.entity.highlightIndex)
			{
				threeMaterial.color = Color.interpolate(0xFFFFFF, 0x0095ff, 0.5);
			}
			const material = {
				signature: pm.signature,
				threeMaterial: threeMaterial
			};
			this.materials.push(material);
		}

		// update the material parameter of the mesh
		if (this.mesh)
		{
			if (this.entity.form === SimplePrimitiveForm.SPHERE)
			{
				// sphere only uses a single material, not an array
				this.mesh.material = this.materials[0].threeMaterial;
			}
			else
			{
				this.mesh.materials = this.materials.map((material): IMaterial => material.threeMaterial as IMaterial);
			}
		}
	}

	protected cacheEntity(): void
	{
		this.cached = {
			width: this.entity.width,
			height: this.entity.height,
			length: this.entity.length,
			highlightIndex: this.entity.highlightIndex
		};
	}

	public redraw(): void
	{
		super.redraw();
	}

	protected updateMesh(): void
	{
		// update the materials only if the materials have changed
		if (this.hasMaterialChanged())
		{
			this.createMaterials();
			this.cacheEntity();
		}

		// update the mesh only if the dimensions have changed
		if (this.hasEntityChanged())
		{
			this.createMesh();
			this.cacheEntity();
		}
		this.shapeBuilder.updateMesh3D(this.mesh);
		this._container.position.set(this._entity.x, this._entity.z, this._entity.y);
		this._container.rotation.set(0, -this.entity.rotation * GeomUtils.D2R, 0);
	}

	private hasMaterialChanged(): boolean
	{
		let materialChanged = false;

		for (const [index, pm] of this.entity.materials.entries())
		{
			if (pm.signature !== this.materials[index]?.signature)
			{
				materialChanged = true;
			}
		}

		return materialChanged || this.cached.highlightIndex !== this.entity.highlightIndex;
	}
	
	private hasEntityChanged(): boolean
	{
		const dimensionsChanged = 
			this.cached.width !== this.entity.width ||
			this.cached.height !== this.entity.height ||
			this.cached.length !== this.entity.length;
			

		return dimensionsChanged;
	}

	public dispose(): void
	{
		this.materials = [];
		this._container.remove(this.mesh);
		this.mesh.dispose();
		super.dispose();
	}
}
