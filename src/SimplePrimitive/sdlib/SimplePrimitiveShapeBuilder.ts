import {
	IAdapter,
	IMaterial,
	IMesh
} from "sdlib";
import {
	SimplePrimitive,
	SimplePrimitiveForm
} from "./SimplePrimitive";

export abstract class SimplePrimitiveShapeBuilder
{
	protected adapter: IAdapter;
	protected simplePrimitive: SimplePrimitive;

	public constructor(adapter: IAdapter, SimplePrimitive: SimplePrimitive)
	{
		this.adapter = adapter;
		this.simplePrimitive = SimplePrimitive;
	}

	public static getShapeBuilder(adapter: IAdapter, SimplePrimitive: SimplePrimitive): SimplePrimitiveShapeBuilder
	{
		if (SimplePrimitive.form === SimplePrimitiveForm.CYLINDER)
		{
			return new CylinderShapeBuilder(adapter, SimplePrimitive);
		}
		else if (SimplePrimitive.form === SimplePrimitiveForm.SPHERE)
		{
			return new SphereShapeBuilder(adapter, SimplePrimitive);
		}
		
		return new BoxShapeBuilder(adapter, SimplePrimitive);
		
	}

	public abstract createMesh2D(material: IMaterial): IMesh;
	public abstract updateMesh2D(mesh: IMesh): void;

	public abstract createMesh3D(materials: IMaterial[]): IMesh;
	public abstract updateMesh3D(mesh: IMesh): void;
}

class BoxShapeBuilder extends SimplePrimitiveShapeBuilder
{
	public constructor(adapter: IAdapter, SimplePrimitive: SimplePrimitive)
	{
		super(adapter, SimplePrimitive);
	}

	public createMesh2D(material: IMaterial): IMesh
	{
		// create a plane buffer geometry with size 1
		const geometry = this.adapter.factory.createPlaneGeometry(1, 1);

		// update the uv attributes to properly fit the texture for top view
		const attribute = geometry.getAttribute('uv');
		for (let i = 0; i < attribute.count; ++i)
		{
			const uv = this.adapter.factory.createVector2(attribute.getX(i), attribute.getY(i));
			attribute.setXY(i, uv.x * this.simplePrimitive.width, uv.y * this.simplePrimitive.length);
		}

		return this.adapter.factory.createMesh(
			geometry,
			material
		);
	}

	public updateMesh2D(mesh: IMesh): void
	{
		// scale the plane mesh
		mesh.scale.set(this.simplePrimitive.width, this.simplePrimitive.length, 1);
	}

	public createMesh3D(materials: IMaterial[]): IMesh
	{
		// create a box with size 1
		const geometry = this.adapter.factory.createBoxGeometry(1, 1, 1);

		// there are 36 indices, every 6 of them represents a face of the cube
		// add material groups to geometry so each face can have a separate material
		for (let i = 0; i < geometry.getIndex().count; i += 6)
		{
			geometry.addGroup(i, 6, i / 6);
		}

		// update the uv attributes to properly fit the textures for all sides
		const attribute = geometry.getAttribute('uv');
		for (let i = 0; i < attribute.count; ++i)
		{
			const uv = this.adapter.factory.createVector2(attribute.getX(i), attribute.getY(i));

			switch (Math.floor(i / 4))
			{
				case 0: attribute.setXY(i, uv.x * Math.abs(this.simplePrimitive.length), uv.y * Math.abs(this.simplePrimitive.height)); break; // right
				case 1: attribute.setXY(i, uv.x * Math.abs(this.simplePrimitive.length), uv.y * Math.abs(this.simplePrimitive.height)); break; // left
				case 2: attribute.setXY(i, uv.x * Math.abs(this.simplePrimitive.width), uv.y * Math.abs(this.simplePrimitive.length)); break;  // top
				case 3: attribute.setXY(i, uv.x * Math.abs(this.simplePrimitive.width), uv.y * Math.abs(this.simplePrimitive.length)); break;  // bottom
				case 4: attribute.setXY(i, uv.x * Math.abs(this.simplePrimitive.width), uv.y * Math.abs(this.simplePrimitive.height)); break;  // front
				case 5: attribute.setXY(i, uv.x * Math.abs(this.simplePrimitive.width), uv.y * Math.abs(this.simplePrimitive.height)); break;  // back
			}
		}

		return this.adapter.factory.createMesh(geometry, materials);
	}

	public updateMesh3D(mesh: IMesh): void
	{
		// position the box mesh to match the zero plane in 3D
		mesh.position.set(0, this.simplePrimitive.height / 2, 0);

		// scale the box mesh
		mesh.scale.set(Math.abs(this.simplePrimitive.width), Math.abs(this.simplePrimitive.height), Math.abs(this.simplePrimitive.length));
	}
}

class CylinderShapeBuilder extends SimplePrimitiveShapeBuilder
{
	// smoothness of the cylinder
	private static readonly SEGMENTS = 30;

	public constructor(adapter: IAdapter, SimplePrimitive: SimplePrimitive)
	{
		super(adapter, SimplePrimitive);
	}

	public createMesh2D(material: IMaterial): IMesh
	{
		// create a circle with size 1
		const geometry = this.adapter.factory.createCircleGeometry(1, CylinderShapeBuilder.SEGMENTS);

		// update the uv attributes to properly fit the texture for top view
		const attribute = geometry.getAttribute('uv');
		for (let i = 0; i < attribute.count; ++i)
		{
			const uv = this.adapter.factory.createVector2(attribute.getX(i), attribute.getY(i));
		 	attribute.setXY(i, uv.x * this.simplePrimitive.width, uv.y * this.simplePrimitive.length);
		}

		return this.adapter.factory.createMesh(
			geometry,
			material
		);
	}

	public updateMesh2D(mesh: IMesh): void
	{
		// scale the circle mesh on x and y with radius (e.g. width/2 and/or length/2)
		mesh.scale.set(Math.abs(this.simplePrimitive.width / 2), Math.abs(this.simplePrimitive.length / 2), 1);
	}

	public createMesh3D(materials: IMaterial[]): IMesh
	{
		// create a cylinder with size 1
		const geometry = this.adapter.factory.createCylinderGeometry(1, 1, 1, CylinderShapeBuilder.SEGMENTS);

		// update the uv attributes to properly fit the textures for all sides
		const attribute = geometry.getAttribute('uv');
		const c = 2 * Math.abs(this.simplePrimitive.width / 2) * Math.PI;
		for (let i = 0; i < attribute.count; ++i)
		{
			const uv = this.adapter.factory.createVector2(attribute.getX(i), attribute.getY(i));
			if (i < 2 * (CylinderShapeBuilder.SEGMENTS + 1))
			{
				 // set uv for the side
				attribute.setXY(i, uv.x * c, uv.y * this.simplePrimitive.height);
			}
			else
			{
				 // set uv for the top/bottom circles
				 attribute.setXY(i, uv.x * this.simplePrimitive.width, uv.y * this.simplePrimitive.length);
			}
		}
		return this.adapter.factory.createMesh(geometry, materials);
	}

	public updateMesh3D(mesh: IMesh): void
	{
		// position the cylinder mesh to match the zero plane in 3D
		mesh.position.set(0, this.simplePrimitive.height / 2, 0);

		// a rotation needs to be applied to properly match the expected results in 3D view
		mesh.rotation.set(0, Math.PI / 2, 0);

		// scale the cylinder mesh with radius, and height values
		mesh.scale.set(Math.abs(this.simplePrimitive.width / 2), Math.abs(this.simplePrimitive.height), Math.abs(this.simplePrimitive.length / 2));
	}
}

class SphereShapeBuilder extends SimplePrimitiveShapeBuilder
{
	// smoothness of the sphere
	private static readonly SEGMENTS = 30;

	public constructor(adapter: IAdapter, SimplePrimitive: SimplePrimitive)
	{
		super(adapter, SimplePrimitive);
	}

	public createMesh2D(material: IMaterial): IMesh
	{
		// create a circle with size 1
		const geometry = this.adapter.factory.createCircleGeometry(1, SphereShapeBuilder.SEGMENTS);

		// update the uv attributes to properly fit the texture for top view
		const attribute = geometry.getAttribute('uv');
		for (let i = 0; i < attribute.count; ++i)
		{
			const uv = this.adapter.factory.createVector2(attribute.getX(i), attribute.getY(i));
			attribute.setXY(i, uv.x * this.simplePrimitive.width, uv.y * this.simplePrimitive.length);
		}
		return this.adapter.factory.createMesh(
			geometry,
			material
		);
	}

	public updateMesh2D(mesh: IMesh): void
	{
		// scale the circle mesh on x and y with radius (e.g. width/2 and/or length/2)
		mesh.scale.set(Math.abs(this.simplePrimitive.width / 2), Math.abs(this.simplePrimitive.length / 2), 1);
	}

	public createMesh3D(materials: IMaterial[]): IMesh
	{
		// create a sphere with size 1
		const geometry = this.adapter.factory.createSphereGeometry(1, SphereShapeBuilder.SEGMENTS, SphereShapeBuilder.SEGMENTS);

		// update the uv attributes to properly fit the texture for the sphere
		const attribute = geometry.getAttribute('uv');
		const c = 2 * Math.abs(this.simplePrimitive.width / 2) * Math.PI;
		for (let i = 0; i < attribute.count; ++i)
		{
			const uv = this.adapter.factory.createVector2(attribute.getX(i), attribute.getY(i));
		 	attribute.setXY(i, uv.x * c, uv.y * c);
		}

		return this.adapter.factory.createMesh(geometry, materials[0]);
	}

	public updateMesh3D(mesh: IMesh): void
	{
		// position the sphere mesh to match the zero plane in 3D
		mesh.position.set(0, this.simplePrimitive.height / 2, 0);

		// scale the sphere mesh with radius
		mesh.scale.set(Math.abs(this.simplePrimitive.width / 2), Math.abs(this.simplePrimitive.height / 2), Math.abs(this.simplePrimitive.length / 2));
	}
}
