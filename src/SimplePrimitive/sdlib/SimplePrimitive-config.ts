import {IEntityConfiguration}        from "sdlib";
import {SimplePrimitive}             from "./SimplePrimitive";
import {SimplePrimitiveShape2D}      from "./SimplePrimitiveShape2D";
import {SimplePrimitiveShape3D}      from "./SimplePrimitiveShape3D";
import {SimplePrimitiveHandle2D}     from "./SimplePrimitiveHandle2D";
import {SimplePrimitiveHandle3D}     from "./SimplePrimitiveHandle3D";
import "../assets/css/main.styl";

export const SimplePrimitiveConfig: IEntityConfiguration = {
	type: SimplePrimitive.TYPE,
	entity: SimplePrimitive,
	shape2D: SimplePrimitiveShape2D,
	shape3D: SimplePrimitiveShape3D,
	handle2D: SimplePrimitiveHandle2D,
	handle3D: SimplePrimitiveHandle3D
};
