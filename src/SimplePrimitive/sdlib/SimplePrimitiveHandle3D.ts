import {
	Pointer,
	Stage,
	View,
	DragHandle
}                         from "sdlib";
import {SimplePrimitive}  from "./SimplePrimitive";

export class SimplePrimitiveHandle3D<TView extends View<any, any>> extends DragHandle<SimplePrimitive, TView>
{
	public constructor(entity: SimplePrimitive, stage: Stage<TView>, pointer?: Pointer)
	{
		super(entity, stage, pointer);
	}

	protected updateDragger(pointer: Pointer): void
	{
		if (!this.entity.locked)
		{
			super.updateDragger(pointer);

			// detect and apply snapping positions during dragging
			const {position, rotation} = this.view.snapSpace.snapEntity(this.entity, {
				ignoreEntityUids: [this.entity],
				onlySides: true
			});
			if (position !== null && rotation !== null)
			{
				this.entity.x += position.x;
				this.entity.y += position.y;
				this.entity.rotation -= rotation;
			}
		}
	}
}

