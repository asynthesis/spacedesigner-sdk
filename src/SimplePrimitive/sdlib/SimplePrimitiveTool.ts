import {
	EntityTool,
	IEntityToolParams,
	ISignal,
	Point2D,
	Pointer,
	Stage,
	View2D
} from "sdlib";

import {
	SimplePrimitive,
	SimplePrimitiveForm
} from "./SimplePrimitive";

export interface ISimplePrimitiveToolParams extends IEntityToolParams
{
	form?: SimplePrimitiveForm
	width?: number;
	height?: number;
	length?: number;
	rotation?: number;
}

export class SimplePrimitiveTool extends EntityTool
{
	protected _simplePrimitive: SimplePrimitive;
	protected _form: SimplePrimitiveForm;
	protected _firstPoint: Point2D;

	public initWithView(stage: Stage<View2D>, params: ISimplePrimitiveToolParams = {}): void
	{
		// define which type of primitive should be created
		this._form = params.form;

		// show the red cross
		params.showCross = true;

		super.initWithView(stage, params);
	}

	protected onViewClickStarted(pointer: Pointer, signal: ISignal): void
	{
		if (!pointer.isNormalClick)
		{
			return;
		}

		super.onViewClickStarted(pointer, signal);

		// primitive is created here on the first mouse down, so the user can do a click and drag interaction to draw the entity
		if (!this._simplePrimitive)
		{
			this._firstPoint = this.getFirstPoint();
			this._simplePrimitive = <SimplePrimitive> SimplePrimitive.Init(this._scene, {
				type: SimplePrimitive.TYPE,
				width: 0,
				height: 0,
				length:  0,
				rotation: 0,
				x: this._firstPoint.x,
				y: this._firstPoint.y,
				form: this._form
			});
			this.setIngoredEntity(this._simplePrimitive);
		}
	}

	protected onViewMouseMove(pointer: Pointer, signal: ISignal): void
	{
		super.onViewMouseMove(pointer, signal);

		if (this._simplePrimitive)
		{
			const point = this._currentPoint;

			// update the primitive sizes properly, based on mouse position
			if (this._simplePrimitive.form === SimplePrimitiveForm.CYLINDER || this._simplePrimitive.form === SimplePrimitiveForm.SPHERE)
			{
				// update the radius, center point is always the first click
				const radius = Math.sqrt(Math.pow(point.x - this._simplePrimitive.x, 2) + Math.pow(point.y - this._simplePrimitive.y, 2));
				this._simplePrimitive.width = radius * 2;
				this._simplePrimitive.length = radius * 2;
			}
			else
			{
				// update the box size and position properly, the first click is the corner of the box
				const width = point.x - this._firstPoint.x;
				const length = point.y - this._firstPoint.y;
				this._simplePrimitive.width = Math.abs(width);
				this._simplePrimitive.length =  Math.abs(length);

				// position should be the center of the box
				this._simplePrimitive.x = this._firstPoint.x + width / 2;
				this._simplePrimitive.y = this._firstPoint.y + length / 2;
			}

			// some logic to calculate a default height based width and lenght values
			this._simplePrimitive.height = Math.round((Math.abs(this._simplePrimitive.width) + Math.abs(this._simplePrimitive.length)) / 2);
		}
	}

	protected onViewClickEnded(pointer: Pointer, signal: ISignal): void
	{
		if (!pointer.isNormalClick)
		{
			return;
		}

		super.onViewClickEnded(pointer, signal);

		const distance = this.view.unprojectPointerToFloor(pointer.pointerData).subtract(this._firstPoint).sqrMagnitude();

		// to avoid early creation on fast mouse release, we use a treshold value for the interaction
		if (distance > 2500)
		{
			// end interaction
			this.dispose();
		}
	}
}
