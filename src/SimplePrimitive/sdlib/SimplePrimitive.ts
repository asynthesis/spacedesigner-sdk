import {
	Box3D,
	BoxEntity,
	ExtendTo,
	GeomUtils,
	IBoundingBoxData,
	IBoxEntityData,
	ISnappable,
	ITransformable,
	Line2D,
	NamedPoint,
	Material,
	Point2D,
	SnapLine,
	SnapSpace2D,
	snappable,
	Scene
} from "sdlib";
import {
	ISimplePrimitiveMaterialData,
	SimplePrimitiveMaterial,
	SimplePrimitiveMaterialType
} from "./SimplePrimitiveMaterial";

export enum SimplePrimitiveForm
	{
	BOX = 'box',
	CYLINDER = 'cylinder',
	SPHERE = 'sphere'
}

export interface ISimplePrimitiveData extends IBoxEntityData
{
	form: SimplePrimitiveForm;
	materials?: ISimplePrimitiveMaterialData[];
}

// use snappable decorator and ISnappable interface to enable other entities to snap to this entity
@snappable
export class SimplePrimitive extends BoxEntity implements ITransformable, ISnappable
{
	private _form: SimplePrimitiveForm = SimplePrimitiveForm.BOX;

	// materials used by this entity, can be a material or an image
	private readonly _materials: SimplePrimitiveMaterial[] = [];

	// runtime variable, a side of the primitive can be higlighted in 3d view hovering the simple primitive panel material selector
	private _highlightIndex: number = -1;	

	public static TYPE = "SimplePrimitive";

	public static Init(scene: Scene, data?: ISimplePrimitiveData): SimplePrimitive
	{
		return <SimplePrimitive> super.Init(scene, data);
	}

	public serialize(): ISimplePrimitiveData
	{
		const data: ISimplePrimitiveData = super.serialize() as ISimplePrimitiveData;

		data.form = this._form;

		data.materials = [];
		for (const pm of this._materials)
		{
			data.materials.push({
				type: pm.type,
				materialData: pm.material.serialize(),
				imageKey: pm.imageKey,
				thumbnailKey: pm.thumbnailKey,
				zoom: pm.zoom
			} as ISimplePrimitiveMaterialData);
		}

		return data;
	}

	public deserialize(data?: ISimplePrimitiveData): void
	{
		super.deserialize(data);

		this._form = data.form;

		if (data.materials)
		{
			// create the materials if available in the data
			for (const material of data.materials)
			{
				this._materials.push(new SimplePrimitiveMaterial(material));
			}
		}
		else
		{
			// create the default materials
			const materialNum = data.form === SimplePrimitiveForm.BOX ? 6 : data.form === SimplePrimitiveForm.CYLINDER ? 3 : 1;
			for (let i = 0; i < materialNum; ++i)
			{
				const material = new SimplePrimitiveMaterial({
					type: SimplePrimitiveMaterialType.MATERIAL,
					materialData: { id: this.scene.mainParams.materials_primitive },
					imageKey: "",
					thumbnailKey: "",
					zoom: 100
				});
				this._materials.push(material);
			}
		}
	
		this.invalidate();
	}

	public get snapLines(): SnapLine[]
	{
		// define the snap lines for other entities for snapping
		const {x, y, width, height, rotation} = this.getBoundingBoxData();
		const center = new Point2D(x, y);

		// corner points
		const tl = new Point2D(-width / 2, -height / 2).rotateOrigin(rotation).add(center);
		const tr = new Point2D(width / 2, -height / 2).rotateOrigin(rotation).add(center);
		const bl = new Point2D(-width / 2, height / 2).rotateOrigin(rotation).add(center);
		const br = new Point2D(width / 2, height / 2).rotateOrigin(rotation).add(center);

		// add the bounding box lines as snaplines
		const line1 = new SnapLine(new Line2D(tl, tr), this, "line1", ExtendTo.Point, SnapSpace2D.PRIORITY_NORMAL, true);
		const line2 = new SnapLine(new Line2D(tr, br), this, "line2", ExtendTo.Point, SnapSpace2D.PRIORITY_NORMAL, true);
		const line3 = new SnapLine(new Line2D(br, bl), this, "line3", ExtendTo.Point, SnapSpace2D.PRIORITY_NORMAL, true);
		const line4 = new SnapLine(new Line2D(bl, tl), this, "line4", ExtendTo.Point, SnapSpace2D.PRIORITY_NORMAL, true);

		// define the mid lines of this entity using corner points
		const tm = new Point2D((tl.x + tr.x) / 2, (tl.y + tr.y) / 2);
		const ml = new Point2D((tl.x + bl.x) / 2, (tl.y + bl.y) / 2);
		const mr = new Point2D((tr.x + br.x) / 2, (tr.y + br.y) / 2);
		const bm = new Point2D((bl.x + br.x) / 2, (bl.y + br.y) / 2);

		// add the mid lines as snaplines
		const line5 = new SnapLine(new Line2D(tm, bm), this, "mid-line-vertical", ExtendTo.Point, SnapSpace2D.PRIORITY_NORMAL, true);
		const line6 = new SnapLine(new Line2D(ml, mr), this, "mid-line-horizontal", ExtendTo.Point, SnapSpace2D.PRIORITY_NORMAL, true);

		// return all the lines that needs to be snapped to
		return [line1, line2, line3, line4, line5, line6];
	}

	public get snapPoints(): NamedPoint[]
	{
		// no special snap points defined for this entity
		return [];
	}

	public get form(): SimplePrimitiveForm
	{
		return this._form;
	}

	// setters, getters

	public set highlightIndex(index: number)
	{
		this._highlightIndex = index;
		this.invalidate();
	}

	public get highlightIndex(): number
	{
		return this._highlightIndex;
	}

	public get materials(): SimplePrimitiveMaterial[]
	{
		return this._materials;
	}

	public setMaterial(material: Material, index: number): void
	{
		if (this._materials[index])
		{
			this._materials[index].material = material;
		}
		this.invalidate();
	}

	public setMaterialType(type: SimplePrimitiveMaterialType, index: number): void
	{
		this._materials[index].type = type;
		this.invalidate();
	}

	public setPictureKeys(imageKey: string, thumbnailKey: string, index: number): void
	{
		this._materials[index].imageKey = imageKey;
		this._materials[index].thumbnailKey = thumbnailKey;
		this.invalidate();
	}

	public setPictureZoom(zoom: number, index: number): void
	{
		this._materials[index].zoom = zoom;
		this.invalidate();
	}

	public getBoundingBoxData(angle: number = 0): IBoundingBoxData
	{
		if (this.form === SimplePrimitiveForm.BOX)
		{
			return <IBoundingBoxData> {
				x: this.x,
				y: this.y,
				width: this.width,
				height: this.length,
				rotation: (this.rotation - angle) * GeomUtils.D2R
			};
		}
		else if (this.form === SimplePrimitiveForm.CYLINDER || this.form === SimplePrimitiveForm.SPHERE)
		{
			return <IBoundingBoxData> {
				x: this.x,
				y: this.y,
				width: this.width,
				height: this.length,
				rotation: (this.rotation - angle) * GeomUtils.D2R
			};
		}
	}

	public getBoundingBox3D(): Box3D
	{
		const box = new Box3D();
		box.rotation[1] = -this.rotation;
		if (this.form === SimplePrimitiveForm.BOX)
		{
			box.width = this.width;
			box.height = this.height;
			box.length = this.length;
			box.setPosition(this.x - this.width / 2, this.z, this.y - this.length / 2);
		}
		else if (this.form === SimplePrimitiveForm.CYLINDER)
		{
			box.width = this.width;
			box.height = this.height;
			box.length = this.length;
			box.setPosition(this.x - this.width, this.z, this.y - this.length);
		}
		else if (this.form === SimplePrimitiveForm.SPHERE)
		{
			box.width = this.width;
			box.height = this.height;
			box.length = this.length;
			box.setPosition(this.x - this.width, this.z, this.y - this.length);
		}
		return box;
	}
}
