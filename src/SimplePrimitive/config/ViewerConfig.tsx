import {EntityConfiguration} from "sdlib";
import {SimplePrimitiveConfig}      from "../sdlib/SimplePrimitive-config";

// Register entity
EntityConfiguration.addEntityConfiguration(SimplePrimitiveConfig);
