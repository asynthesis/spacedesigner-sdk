import * as React                from "react";
import {C, EntityConfiguration}  from "sdlib";
import {EntityPanels, LeftPanel} from "spacedesigner";
import {SimplePrimitivePanel}    from "../sdeditor/SimplePrimitivePanel";
import {SimplePrimitiveTab}      from "../sdeditor/SimplePrimitiveTab";
import {SimplePrimitive}         from "../sdlib/SimplePrimitive";
import {SimplePrimitiveConfig}   from "../sdlib/SimplePrimitive-config";

// Register entity
EntityConfiguration.addEntityConfiguration(SimplePrimitiveConfig);

// Register property panel
EntityPanels.setPanelForEntity(SimplePrimitive.TYPE, (entity: SimplePrimitive) => <SimplePrimitivePanel entity={entity} key={entity.uid}/>);

// Register left panel
LeftPanel.addPanel("simpleprimitive", C("simple primitive short label"), <SimplePrimitiveTab/>);
