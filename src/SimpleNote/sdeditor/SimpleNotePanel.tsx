import * as React          from "react";
import {C}                 from "sdlib";
import {
	ColorInput,
	EditorMain,
	Label,
	NumericStepper,
	PropanelContainer,
	TextArea
}                          from "spacedesigner";
import {SimpleNote}        from "../sdlib/SimpleNote";

interface ISimpleNotePanelProps
{
	entity: SimpleNote;
}

interface ISimpleNotePanelState
{
	text: string;
	backgroundColor: number;
	backgroundOpacity: number;
}

export class SimpleNotePanel extends React.PureComponent<ISimpleNotePanelProps, ISimpleNotePanelState>
{
	public constructor(props: ISimpleNotePanelProps)
	{
		super(props);

		const entity = this.props.entity;

		this.state = {
			text: entity.text,
			backgroundColor: entity.backgroundColor,
			backgroundOpacity: entity.backgroundOpacity
		};
	}

	private readonly onTextChange = (e: React.ChangeEvent<HTMLInputElement>): void =>
	{
		const text = e.currentTarget.value;
		this.props.entity.text = text;
		this.setState({text: text});
	};

	private readonly onBackgroundColorChange = (value: number): void =>
	{
		this.props.entity.backgroundColor = value;
		this.setState({backgroundColor: value});
	};

	private readonly onBackgroundOpacityChange = (value: number): void =>
	{

		this.props.entity.backgroundOpacity = value;
		this.setState({backgroundOpacity: value});
	};

	public render(): JSX.Element
	{
		const {entity} = this.props;
		const params = EditorMain.instance.params;
		return (
			<PropanelContainer
				className="sd-abstractpanel sd-simplenote-panel"
				entity={entity}
				enableDuplicate={true}
				enableDelete={true}
			>
				<div className="title">{C("simple note entity")}</div>
				<TextArea onChange={this.onTextChange} value={this.state.text} rows={3} readOnly={false}/>

				<div className="hbox">
					<Label text={C('annotation background')}/>
					<ColorInput value={this.state.backgroundColor} onChange={this.onBackgroundColorChange}/>
					<NumericStepper
						onChange={this.onBackgroundOpacityChange}
						min={0}
						max={1}
						stepSize={0.1}
						value={this.state.backgroundOpacity}
						assetsUrl={params.url_ui_assets}
					/>
				</div>
			</PropanelContainer>
		);
	}
}
