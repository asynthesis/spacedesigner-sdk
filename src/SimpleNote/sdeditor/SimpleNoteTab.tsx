import * as React           from 'react';
import {C}                  from "sdlib";
import {Button, EditorMain} from "spacedesigner";
import {
	ISimpleNoteToolParams,
	SimpleNoteTool
}                           from "../sdlib/SimpleNoteTool";

export interface ISimpleNoteTabProps
{
}

export interface ISimpleNoteTabStates
{
}

export class SimpleNoteTab extends React.PureComponent<ISimpleNoteTabProps, ISimpleNoteTabStates>
{

	public render(): JSX.Element
	{
		return (
			<div className="sd-simplenote-tab">
				<div className="leftpanel-padding">
					<span className="title1">{C('simple note tab')}</span>
					<br/>
					<Button
						cls="secondary"
						label={C("create simple note")}
						onInteract={() =>
						{
							const params = {
								backgroundColor: 0xdbffe4,
								backgroundOpacity: 0.7,
								text: "my fist note"
							} as ISimpleNoteToolParams;
							EditorMain.instance.getView().initTool(SimpleNoteTool, params);
						}}
					/>
				</div>
			</div>
		);
	}
}
