import {EntityConfiguration}   from "sdlib";
import {SimpleNoteConfig}      from "../sdlib/SimpleNote-config";

// Register entity
EntityConfiguration.addEntityConfiguration(SimpleNoteConfig);
