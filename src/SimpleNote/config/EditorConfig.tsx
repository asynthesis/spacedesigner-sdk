import * as React                from "react";
import {C, EntityConfiguration}  from "sdlib";
import {EntityPanels, LeftPanel} from "spacedesigner";
import {SimpleNotePanel}         from "../sdeditor/SimpleNotePanel";
import {SimpleNoteTab}           from "../sdeditor/SimpleNoteTab";
import {SimpleNote}              from "../sdlib/SimpleNote";
import {SimpleNoteConfig}        from "../sdlib/SimpleNote-config";

// Register entity
EntityConfiguration.addEntityConfiguration(SimpleNoteConfig);

// Register property panel
EntityPanels.setPanelForEntity(SimpleNote.TYPE, (entity: SimpleNote) => <SimpleNotePanel entity={entity} key={entity.uid}/>);

// Register left panel
LeftPanel.addPanel("simplenote", C("simple note short label"), <SimpleNoteTab/>);
