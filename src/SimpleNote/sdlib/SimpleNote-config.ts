import {IEntityConfiguration}   from "sdlib";
import {SimpleNote}             from "./SimpleNote";
import {SimpleNoteHandle2D}     from "./SimpleNoteHandle2D";
import {SimpleNoteShape2D}      from "./SimpleNoteShape2D";
import {SimpleNoteShape3D}      from "./SimpleNoteShape3D";
import "../assets/css/main.styl";

export const SimpleNoteConfig: IEntityConfiguration = {
	// entity type string to identify the entity class
	type: SimpleNote.TYPE,

	// entity class itself
	entity: SimpleNote,

	// entity shape 2D class to use with view 2D
	shape2D: SimpleNoteShape2D,

	// entity shape 3D class to use with view 3D
	shape3D: SimpleNoteShape3D,

	// entity handle 2D class to use with view 2D when entity is selected
	handle2D: SimpleNoteHandle2D
};
