import {
	GeomUtils,
	DomSprite
}                   from "sdlib";
import {SimpleNote} from "./SimpleNote";


export class SimpleNoteShapeCommon
{
	/**
	 * Apply label styling to the domsprite. Similar to applyLabelDivStyle but for DomSprite-s.
	 * Also might be called from the 3D shape for consistency.
	 *
	 * @param entity The annotation
	 * @param label The domsprite for the label
	 */
	public static applyLabelStyle(entity: SimpleNote, label: DomSprite): void
	{
		const labelDiv = label.element;

		labelDiv.innerHTML = entity.parsedText;
		labelDiv.style.color = "black";
		labelDiv.style.fontWeight = "normal";
		labelDiv.style.fontStyle = "normal";
		labelDiv.style.textAlign = "center";

		labelDiv.style.fontSize = "18px";
		labelDiv.style.padding = "5px";
		 const {x, y, width, height} = SimpleNoteShapeCommon.getTransformation(entity);
 
		 const [usedW, usedH] = GeomUtils.fitSize([width, height], [500, 500]);
		 const labelScale = width / usedW;
 
 
		 label.setSize(usedW, usedH);
		 label.setScale(labelScale, height / usedH, 1);
 
		 label.setPos(x + width / 2, y + height / 2);
		 label.rotation = entity.rotation;
	}
 
	/**
	 * Gets the 2D (top-down) width, height, x, y values for the SimpleNote entity
	*/
	public static getTransformation(entity: SimpleNote): { width: number; height: number; x: number; y: number }
	{
		let width = entity.width;
		let height = entity.height;
		let x = entity.x;

		if (width < 0)
		{
			x += width;
			width = Math.abs(width);
		}
	
		let y = entity.y;
	
		if (height < 0)
		{
			y += height;
			height = Math.abs(height);
		}
	
		return {
			width, height, x, y
		};
	}
}