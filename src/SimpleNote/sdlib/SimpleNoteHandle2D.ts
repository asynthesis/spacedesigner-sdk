import {
	DomSprite,
	DragHandle,
	GeomUtils,
	Point2D,
	Pointer,
	Stage,
	View2D
}                    from "sdlib";
import {SimpleNote}  from "./SimpleNote";

export class SimpleNoteHandle2D extends DragHandle<SimpleNote, View2D>
{
	protected corners: DomSprite[] = [];
	private activeCorner = -1;

	public constructor(entity: SimpleNote, stage: Stage<View2D>, pointer?: Pointer)
	{
		super(entity, stage, pointer);

		const editable = this.view.scene.mainParams.editable;

		// create 2D handle , use the rotation symbol if editable view(editor) is loaded
		this.create2DUI(editable, false, false);

		// create corner points for resize if editable view(editor) is loaded
		if (editable)
		{
			this._pivotSign.element.style.visibility = "hidden";
			this.createCorners();
		}
	}

	private createCorners(): void
	{
		// create corners for the rectangle resize
		this.entity.points.forEach((point: Point2D, i: number): void =>
		{
			// create a corner dom element, style has the default corner icon, but can be changed
			const newSprite = this.adapter.factory.createDomSprite(`<div class="simpleNoteCorner"></div>`);
			newSprite.setPos(point.x, point.y);
			newSprite.setScale(1 / this.view.scale);
			this.view.dom.add(newSprite);
			this.corners.push(newSprite);

			// set up corner mouse click callbacks
			newSprite.signals.down.add((pointer: Pointer): void => this.onCornerMouseDown(pointer, i));
			newSprite.signals.up.add((): void => this.onCornerMouseUp(i));
		});
	}

	private onCornerMouseDown(pointer: Pointer, cornerIndex: number): void
	{
		// register corner mouse click
		this.activeCorner = cornerIndex;
		this.corners[cornerIndex].signals.move.add(this.onCornerMouseMove, this);
		pointer.originalEvent.stopImmediatePropagation();
	}

	private onCornerMouseMove(pointer: Pointer): void
	{
		// handle corner mouse move
		const newPosition = this.view.snapSpace.trySnappingPointer([], pointer, {}, false);
		this.entity.movePoint(this.activeCorner, newPosition);
	}

	private onCornerMouseUp(cornerIndex: number): void
	{
		// finish corner mosue move handling
		this.activeCorner = -1;
		this.corners[cornerIndex].signals.move.remove(this.onCornerMouseMove, this);
	}

	protected onRotationStart(pointer: Pointer): void
	{
		// stop event from propagating to view (which would pan the camera or select);
		pointer.originalEvent.stopImmediatePropagation();

		this._lastAngle = this.getMouseAngle(pointer);

		this.pauseBoundaryManager();
	}

	protected onRotationMove(pointer: Pointer): void
	{
		const mouseAngle = this.getMouseAngle(pointer);

		// Round to 5 integers (0, 5, 10, etc)
		let roundTo = 5;

		if (pointer.originalEvent.shiftKey)
		{
			roundTo = 1;
		}

		const diff = Math.round((mouseAngle - this._lastAngle) / roundTo) * roundTo;
		if (diff !== 0)
		{
			this.entity.rotate(diff, this.entity.center);
			this._lastAngle = mouseAngle;
		}
	}

	protected getMouseAngle(pointer: Pointer): number
	{
		const mousePos = this.view.unprojectPointerToFloor(pointer.pointerData);

		const dx = mousePos.x - this.entity.x - this.entity.width / 2;
		const dy = mousePos.y - this.entity.y - this.entity.height / 2;

		return Math.atan2(dy, dx) * GeomUtils.R2D;
	}

	public redraw(): void
	{
		super.redraw();

		const entity = this.entity;
		const scale = 1 / this.view.scale;

		this.entity.points.forEach((point: Point2D, i: number): void =>
		{
			const corner = this.corners[i];
			corner.setPos(point.x, point.y);
			corner.setScale(scale);
		});

		// position rotation symbol properly
		if (this._rotationSymbol)
		{
			const origin = new Point2D(0, 0);
			const sign = (entity.height >= 0) ? -1 : 1;
			const offset = origin.offset(sign * scale * 20 - entity.height / 2, entity.rotation * GeomUtils.D2R);
			this._rotationSymbol.setPos(entity.x + entity.width / 2 + offset.x, entity.y + entity.height / 2 + offset.y);
		}
	}

	private disposeCorners(): void
	{
		for (const corner of this.corners)
		{
			corner.signals.down.removeAll();
			corner.signals.up.removeAll();
			corner.dispose();
		}
		this.corners = [];
	}

	public dispose(): void
	{
		this.disposeCorners();
		super.dispose();
	}
}
