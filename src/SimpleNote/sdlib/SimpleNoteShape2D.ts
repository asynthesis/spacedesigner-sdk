import {
	DomSprite,
	DomUtils,
	EntityShape2D,
	GeomUtils,
	IMesh,
	MeshBasicMaterial,
	Stage,
	View2D
}                               from "sdlib";
import {SimpleNote}             from "./SimpleNote";
import {SimpleNoteShapeCommon}  from "./SimpleNoteShapeCommon";

export class SimpleNoteShape2D extends EntityShape2D<SimpleNote>
{
	protected _noteLabel: DomSprite;
	protected _shape: IMesh;

	public constructor(entity: SimpleNote, stage: Stage<View2D>)
	{
		super(entity, stage);
		this._container = this.adapter.factory.createObject3D();

		// create the note label as a dom element in 2d view
		const div = DomUtils.createDiv();
		div.style.padding = "5px";
		this._noteLabel = new DomSprite(div, this.adapter);

		// create the note background shape as a threejs mesh
		this._shape = this.adapter.factory.createMesh(
			this.adapter.factory.createPlaneGeometry(1, 1),
			this.adapter.factory.createMeshBasicMaterial({
				color: this._entity.backgroundColor,
				transparent: true,
				opacity: Math.max(this._entity.backgroundOpacity, 0.01)  // don't allow fully transparent, click won't work then
			})
		);
		this.stage.add(this._container);
	}

	public redraw(): void
	{
		super.redraw();

		// draw entity if visible
		if (this._entity.isActive && this.entity.visible)
		{
			this.updateMesh();

			// show entity if not present
			if (!this._noteLabel.element.parentElement)
			{
				this.view.dom.add(this._noteLabel);
				this._container.add(this._shape);
			}
		}
		else if (this._noteLabel.element)
		{
			this.view.dom.remove(this._noteLabel);
			this._container.remove(this._shape);
		}

		// set visibility to container as well
		this._container.visible = this.entity.visible;
	}

	protected updateMesh(): void
	{
		const entity = this.entity;

		// apply label style to the label div
		SimpleNoteShapeCommon.applyLabelStyle(this.entity, this._noteLabel);

		// position and scale background shape
		const {x, y, width, height} = SimpleNoteShapeCommon.getTransformation(entity);
		this._shape.scale.set(width, height, 1);
		this._shape.rotation.set(0, 0, -entity.rotation * GeomUtils.D2R);

		// we increment z with 2000 to ensure it's always on top of items
		this._shape.position.set(
			x + width / 2,
			-y - height / 2,
			this._entity.scene.levels.getById(this._entity.level).height + 2000
		);

		// set background shape color and opacity
		(this._shape.material as MeshBasicMaterial).color.set(this._entity.backgroundColor);
		(this._shape.material as MeshBasicMaterial).opacity = Math.max(this._entity.backgroundOpacity, 0.01); // don't allow fully transparent, click won't work then
	}

	public dispose(): void
	{
		this._noteLabel.dispose();

		this._container.remove(this._shape);
		this._shape.dispose();

		super.dispose();
	}
}
