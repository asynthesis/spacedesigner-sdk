import {
	Color,
	DomSprite,
	EntityShape3D,
	Stage,
	View3D
}                               from "sdlib";
import {SimpleNote}             from "./SimpleNote";
import {SimpleNoteShapeCommon}  from "./SimpleNoteShapeCommon";

export class SimpleNoteShape3D extends EntityShape3D<SimpleNote>
{
	// note label will be a dom sprite in 3d view
	protected _noteLabel = new DomSprite(`<div class="boundary-label"></div>`, this.adapter);;

	// eslint-disable-next-line @typescript-eslint/no-useless-constructor
	public constructor(entity: SimpleNote, stage: Stage<View3D>)
	{
		super(entity, stage);
	}

	private get visible(): boolean
	{
		// show label if we are on the active level and version
		return this._entity.isActive && this._entity.visible;
	}

	public redraw(): void
	{
		super.redraw();
		const entity = this.entity;

		// show/hide note label
		if (this.visible)
		{
			this.view.dom.add(this._noteLabel);
		}
		else
		{
			this.view.dom.remove(this._noteLabel);
		}

		// set note label position, scale, text, style
		this._noteLabel.element.innerHTML = entity.parsedText;
		this._noteLabel.object.position.set(entity.x, 0, entity.y);
		this._noteLabel.element.style.backgroundColor = Color.getCSSFromNumber(entity.backgroundColor, entity.backgroundOpacity);
		SimpleNoteShapeCommon.applyLabelStyle(this.entity, this._noteLabel);
	}

	public dispose(): void
	{
		this.view.dom.remove(this._noteLabel);

		super.dispose();
	}
}
