import {
	C,
	EntityTool,
	IEntityToolParams,
	ISignal,
	Pointer,
	Property,
	Stage,
	View2D
}                                      from "sdlib";
import {ISimpleNoteData, SimpleNote}   from "./SimpleNote";

export interface ISimpleNoteToolParams extends IEntityToolParams
{
	backgroundColor?: number;
	backgroundOpacity?: number;
	color?: number;
	width?: number;
	height?: number;
	text?: string;
}

export class SimpleNoteTool extends EntityTool
{
	protected _backgroundColor = new Property<number>(0xf1e05e);
	protected _backgroundOpacity = new Property<number>(0.5);
	protected _color = new Property<number>(0x0);
	protected _width = new Property<number>(1);
	protected _height = new Property<number>(1);
	protected _text = new Property<string>(C("simple note description"));
	protected _thickness = new Property<number>(2);
	protected _note: SimpleNote;

	public initWithView(stage: Stage<View2D>, params: ISimpleNoteToolParams): void
	{
		params.showCross = true;

		this._backgroundColor.apply(params.backgroundColor);
		this._backgroundOpacity.apply(params.backgroundOpacity);
		this._color.apply(params.color);
		this._width.apply(params.width);
		this._height.apply(params.height);
		this._text.apply(params.text);

		super.initWithView(stage, params);
	}

	protected onViewClickStarted(pointer: Pointer, signal: ISignal): void
	{
		// only handle left clicks
		if (!pointer.isNormalClick)
		{
			return;
		}

		super.onViewClickStarted(pointer, signal);
	}

	protected onViewMouseMove(pointer: Pointer, signal: ISignal): void
	{
		super.onViewMouseMove(pointer, signal);

		// update note properties
		if (this._note)
		{
			const point = this._currentPoint;
			const distX = point.x - this._note.x;
			const distY = point.y - this._note.y;

			// if value 0 it kills three.js texture mechanism
			this._note.width = distX || 1;
			this._note.height = distY || 1;
		}
	}

	protected onViewClickEnded(pointer: Pointer, signal: ISignal): void
	{
		if (!pointer.isNormalClick)
		{
			return;
		}

		super.onViewClickEnded(pointer, signal);

		// if note is already created, this is the second click
		if (this._note)
		{
			// end interaction
			this.dispose();
		}
		// create note, this is the first click
		else
		{
			const firstPoint = this.getFirstPoint();
			this._note = SimpleNote.Init(
				this._scene,
				<ISimpleNoteData> {
					type: SimpleNote.TYPE,
					backgroundColor: this._backgroundColor.value,
					backgroundOpacity: this._backgroundOpacity.value,
					color: this._color.value,
					width: this._width.value,
					height: this._height.value,
					text: this._text.value,
					x: firstPoint.x,
					y: firstPoint.y
				}
			);
		}
	}
}
