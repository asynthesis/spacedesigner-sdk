import {
	C,
	IRectangleData,
	Point2D,
	Property,
	Rectangle,
	RichTextUtils,
	Scene
} from "sdlib";

export interface ISimpleNoteData extends IRectangleData
{
	backgroundColor: number;
	backgroundOpacity: number;
	text: string;
}
	
export class SimpleNote extends Rectangle
{
	public static TYPE = "SimpleNote";

	protected _backgroundColor = new Property<number>(0x0);
	protected _backgroundOpacity = new Property<number>(1);
	protected _text = new Property<string>(C("simple note description"));

	/**
	* Init function to create a new instance of the entity.
	* It is usually a super.Init call, and must return the proper entity type. Some custom logic can be added here.
	*
	* @param scene Scene to create the entity.
	* @param data Entity data to create the entity with.
	*/
	public static Init(scene: Scene, data?: ISimpleNoteData): SimpleNote
	{
		return <SimpleNote> super.Init(scene, data);
	}

	/**
	* Clone function to create a copy of the original entity.
	* It must be implemented, or the parent class clone will be called.
	*
	* @param offset Optional offset point to clone to.
	* @param levelId The levelId to clone to.
	* @param versionId The versionId to clone to.
	*/
	public clone(offset?: Point2D, levelId?: string, versionId?: string): SimpleNote
	{
		const data = this.serialize();

		this.scene.ids.transformUids(data);

		if (offset)
		{
			data.x += offset.x;
			data.y += offset.y;
		}

		if (levelId !== undefined)
		{
			data.level = levelId;
		}
		if (versionId !== undefined)
		{
			data.version = versionId;
		}

		return <SimpleNote> SimpleNote.Init(this.scene, data);
	}

	/**
	* Serialize the entity.
	* It must return an entity data that will be saved in the project json.
	*
	*/
	public serialize(): ISimpleNoteData
	{
		const data = <ISimpleNoteData> super.serialize();

		data.backgroundColor = this._backgroundColor.value;
		data.backgroundOpacity = this._backgroundOpacity.value;
		data.text = this._text.value;

		return data;
	}

	/**
	* Deserialize the entity.
	* It reads a data json and the entity is initalized with the values provided.
	* @param data The entity data to read and use.
	*/
	public deserialize(data?: ISimpleNoteData): void
	{
		super.deserialize(data);

		this._backgroundColor.apply(data.backgroundColor);
		this._backgroundOpacity.apply(data.backgroundOpacity);
		this._text.apply(data.text);

		this.invalidate();
	}

	public get backgroundColor(): number
	{
		return this._backgroundColor.value;
	}

	public set backgroundColor(value: number)
	{
		this._backgroundColor.apply(value);
		this.invalidate();
	}

	public get backgroundOpacity(): number
	{
		return this._backgroundOpacity.value;
	}

	public set backgroundOpacity(value: number)
	{
		this._backgroundOpacity.apply(value);
		this.invalidate();
	}

	public get text(): string
	{
		return this._text.value;
	}

	public set text(value: string)
	{
		this._text.apply(value);
		this.invalidate();
	}

	public get parsedText(): string
	{
		return RichTextUtils.UrlToAnchor(this.text);
	}
}
