import {EntityConfiguration} from "sdlib";
import {TemplateConfig}      from "../sdlib/Template-config";

// Register entity
EntityConfiguration.addEntityConfiguration(TemplateConfig);
