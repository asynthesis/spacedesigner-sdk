import * as React                from "react";
import {C, EntityConfiguration}  from "sdlib";
import {EntityPanels, LeftPanel} from "spacedesigner";
import {TemplatePanel}           from "../sdeditor/TemplatePanel";
import {TemplateTab}             from "../sdeditor/TemplateTab";
import {Template}                from "../sdlib/Template";
import {TemplateConfig}          from "../sdlib/Template-config";

// Register entity
EntityConfiguration.addEntityConfiguration(TemplateConfig);

// Register property panel
EntityPanels.setPanelForEntity(Template.TYPE, (entity: Template) => <TemplatePanel entity={entity} key={entity.uid}/>);

// Register left panel
LeftPanel.addPanel("template", C("template short label"), <TemplateTab/>);
