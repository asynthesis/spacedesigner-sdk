import * as React          from "react";
import {C}                 from "sdlib";
import {PropanelContainer} from "spacedesigner";
import {Template}          from "../sdlib/Template";

interface ITemplatePanelProps
{
	entity: Template;
}

interface ITemplatePanelState
{
}

export class TemplatePanel extends React.PureComponent<ITemplatePanelProps, ITemplatePanelState>
{
	public render(): JSX.Element
	{
		const {entity} = this.props;
		return (
			<PropanelContainer
				className="sd-templatepanel"
				entity={entity}
				updateFn={() => this.forceUpdate()}
			>
				<div className="title">{C("template entity")}</div>
			</PropanelContainer>
		);
	}
}
