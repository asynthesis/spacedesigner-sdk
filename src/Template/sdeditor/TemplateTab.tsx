import * as React           from 'react';
import {C}                  from "sdlib";
import {Button, EditorMain} from "spacedesigner";
import {TemplateTool}       from "../sdlib/TemplateTool";

export interface ITemplateTabProps
{
}

export interface ITemplateTabStates
{
}

export class TemplateTab extends React.PureComponent<ITemplateTabProps, ITemplateTabStates>
{

	public render(): JSX.Element
	{
		return (
			<div className="sd-template-tab">
				<div className="leftpanel-padding">
					<span className="title1">{C('template tab')}</span>
					<br/>
					<Button
						cls="secondary"
						label={C("create entity")}
						onInteract={() =>
						{
							const params = {
								template_param: "entity tool test parameter"
							};
							EditorMain.instance.getView().initTool(TemplateTool, params);
						}}
					/>
				</div>
			</div>
		);
	}
}
