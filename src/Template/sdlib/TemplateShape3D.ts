import {EntityShape3D, Stage, View3D} from "sdlib";
import {Template}                     from "./Template";

export class TemplateShape3D extends EntityShape3D<Template>
{
	// eslint-disable-next-line @typescript-eslint/no-useless-constructor
	public constructor(entity: Template, stage: Stage<View3D>)
	{
		super(entity, stage);

		// ...
	}

	public redraw(): void
	{
		super.redraw();

		// ...
	}

	public dispose(): void
	{
		// ...

		super.dispose();
	}
}
