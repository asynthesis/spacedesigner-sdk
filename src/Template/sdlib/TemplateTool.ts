import {EntityTool, IEntityToolParams, ISignal, Pointer, Stage, View2D} from "sdlib";
import {ITemplateData, Template}                                        from "./Template";

export interface ITemplateToolParams extends IEntityToolParams
{
	template_param: string;
	// ...
}

export class TemplateTool extends EntityTool
{
	// ...

	public initWithView(stage: Stage<View2D>, params: ITemplateToolParams): void
	{
		super.initWithView(stage, params);

		// ...
	}

	protected onViewClickStarted(pointer: Pointer, signal: ISignal): void
	{
		if (!pointer.isNormalClick)
		{
			return;
		}

		super.onViewClickStarted(pointer, signal);

		// ...
	}

	protected onViewMouseMove(pointer: Pointer, signal: ISignal): void
	{
		super.onViewMouseMove(pointer, signal);

		// ...
	}

	protected onViewClickEnded(pointer: Pointer, signal: ISignal): void
	{
		if (!pointer.isNormalClick)
		{
			return;
		}

		super.onViewClickEnded(pointer, signal);

		// Create entity on mouse click at given position
		const position = this.view.unprojectPointerToFloor(pointer.pointerData);
		const templateData: ITemplateData = {
			type: Template.TYPE,
			level: this._scene.levels.active.uid,
			version: this._scene.versions.active.uid,
			x: position.x,
			y: position.y,
			z: 0,
			width: 100,
			length: 100,
			height: 100
		};
		Template.Init(this._scene, templateData);
	}
}
