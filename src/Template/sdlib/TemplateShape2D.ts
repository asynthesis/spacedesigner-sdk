import {EntityShape2D, Stage, View2D} from "sdlib";
import {Template}                     from "./Template";

export class TemplateShape2D extends EntityShape2D<Template>
{
	// eslint-disable-next-line @typescript-eslint/no-useless-constructor
	public constructor(entity: Template, stage: Stage<View2D>)
	{
		super(entity, stage);

		// ...
	}

	public redraw(): void
	{
		super.redraw();

		// ...
	}

	public dispose(): void
	{
		// ...

		super.dispose();
	}
}
