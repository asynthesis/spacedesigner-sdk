import {DragHandle, Pointer, Stage, View2D} from "sdlib";
import {Template}                           from "./Template";

export class TemplateHandle2D extends DragHandle<Template, View2D>
{
	// eslint-disable-next-line @typescript-eslint/no-useless-constructor
	public constructor(entity: Template, stage: Stage<View2D>, pointer?: Pointer)
	{
		super(entity, stage, pointer);

		// ...
	}

	public redraw(): void
	{
		super.redraw();

		// ...
	}

	public dispose(): void
	{
		// ...

		super.dispose();
	}
}
