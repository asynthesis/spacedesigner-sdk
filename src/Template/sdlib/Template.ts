import {BoxEntity, Scene} from "sdlib";

export interface ITemplateData
{
	// ...
}

export class Template extends BoxEntity
{
	public static TYPE = "Template";

	// ...

	public static Init(scene: Scene, data?: ITemplateData): Template
	{
		return <Template> super.Init(scene, data);
	}

	public serialize(): ITemplateData
	{
		const data = <ITemplateData> super.serialize();

		// ...

		return data;
	}

	public deserialize(data?: ITemplateData): void
	{
		super.deserialize(data);

		// ...

		this.invalidate();
	}
}
