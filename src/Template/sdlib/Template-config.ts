import {IEntityConfiguration} from "sdlib";
import {Template}             from "./Template";
import {TemplateHandle2D}     from "./TemplateHandle2D";
import {TemplateShape2D}      from "./TemplateShape2D";
import {TemplateShape3D}      from "./TemplateShape3D";
import "../assets/css/main.styl";

export const TemplateConfig: IEntityConfiguration = {
	type: Template.TYPE,
	entity: Template,
	shape2D: TemplateShape2D,
	shape3D: TemplateShape3D,
	handle2D: TemplateHandle2D
};
